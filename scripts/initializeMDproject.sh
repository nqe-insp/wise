#!/bin/bash

canvasDir="$( cd "$(dirname "$0")"/.. >/dev/null 2>&1 ; pwd -P )"
upstreamURL="$( cd "$(dirname "$0")"/.. >/dev/null 2>&1 ; git remote get-url origin )"

if [ -z $1 ]; then
  echo "project name must be specified."
  exit 1
fi

if [ -a $1 ]; then
  echo $1" already exists!"
  exit 1
fi

git clone $canvasDir $1 || exit 1
cd $1
git remote remove origin
git remote add mdcanvas $upstreamURL
git remote set-url --push mdcanvas DISABLE
cd source
sed -i'.bak' -e 's/World/'$1'/' main.f90
rm main.f90.bak
cd ..
git add .
git commit -m "initialized from mdcanvas"
cd ..

