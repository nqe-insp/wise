PROGRAM WiSE
	use kinds
	use nested_dictionaries
	use random
	use file_handler
	use atomic_units
	use string_operations
	use timer_module
	use framework_initialization, only: initialize_framework,&
                                      open_output_files
  use system_commons
  use wigner_forces
  use potential_dispatcher
  use potential
  use wigner_sampler
  implicit none
	type(DICT_STRUCT) :: param_library	
  real(dp) :: dt_dyn, gamma_dyn, spectrum_cutoff
  real(dp), allocatable :: X(:,:), P(:,:), Pw(:,:,:)
  real(dp) :: kinWeight, kinWeight_avg
  integer :: nsteps_dyn, nsteps_btwn_samples, nsamples
  integer :: n_momenta_per_sample
  integer :: nsteps_aux_quick, nsteps_aux_quick_therm
  integer :: nsteps_aux_precise, nsteps_aux_precise_therm
  integer :: nsteps_penalty_btwn_samples, nsteps_penalty_therm
  logical :: penalty_correction
  integer :: num_aux_sim
  integer :: isample,ux,up,uw,ip,uk2,uk2var
  integer :: write_stride
  type(WIG_AUX_SIM_TYPE) :: auxsim
  logical :: verbose
  real :: momentum_time_avg,dynamics_time_avg,lgv_time_avg, remaining_time, job_time
  real :: step_time_avg,step_cpu_time_avg,step_cpu_time_start,step_cpu_time_finish
  integer :: avg_counter
  real(dp) :: accratio_penalty_avg, eta_penalty_avg, eta_too_big_ratio_avg
  logical :: sample_momentum_precise=.TRUE.
  integer :: n_threads, cpp_counter
  real(dp), allocatable :: Cpp_avg(:,:), Cpp(:,:,:), mCvv_avg(:,:)
  real(dp), allocatable :: Cmumu_avg(:,:), Cmumu(:,:,:)
  TYPE(timer_type) :: full_timer,step_timer,full_step_timer
  real :: cpu_time_start,cpu_time_finish
  real(dp) :: Qtemp_avg,Qekin, pot_avg
  real(dp), allocatable :: Qtemp(:)
  call full_timer%start
  call cpu_time(cpu_time_start)


  call initialize_framework(param_library)	

  ! GET NUMBER OF THREADS        
  n_threads=1
  !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

  !----------------------------------------
  ! SIMULATION INITIALIZATION GOES HERE
  call initialize_system(param_library)
  call initialize_potential(param_library,nat,ndim)
  call initialize_wigner_commons(param_library)  

  write_stride = param_library%get("parameters/write_stride",default=1)
  avg_counter = 0
  momentum_time_avg = 0.
  dynamics_time_avg = 0.
  lgv_time_avg = 0.
  step_time_avg = 0.


  n_momenta_per_sample = param_library%get("parameters/n_momenta_per_sample",default=1)
  allocate(X(nat,ndim), P(nat,ndim), Pw(nat,ndim,n_momenta_per_sample))
  allocate(Qtemp(n_momenta_per_sample))
  X(:,:) = Xinit(:,:)
  call sample_classical_momentum(P)
  Pw(:,:,:) = 0._dp  
  kinWeight_avg=0._dp
  Qtemp_avg = 0._dp
  pot_avg = 0._dp


  ! INITIALIZE SAMPLER 
  nsteps_btwn_samples = param_library%get("sampler/nsteps_btwn_samples")
  nsamples = param_library%get("parameters/nsamples")  
  call initialize_sampler(param_library)

  !INITIALIZE DYNAMICS
  nsteps_dyn = param_library%get("dynamics/nsteps")
  dt_dyn = param_library%get("dynamics/dt")
  gamma_dyn = param_library%get("dynamics/gamma",default=-1._dp)
  spectrum_cutoff = param_library%get("dynamics/spectrum_cutoff",default=-1._dp)
  allocate(Cpp_avg(ndof,nsteps_dyn), Cpp(ndof,nsteps_dyn,n_momenta_per_sample),mCvv_avg(ndim,nsteps_dyn))
  Cpp_avg(:,:)=0._dp
  cpp_counter = 0
  if(charges_provided) then
    allocate(Cmumu_avg(ndim,nsteps_dyn), Cmumu(ndim,nsteps_dyn,n_momenta_per_sample))
    Cmumu_avg = 0._dp
  endif
  

  ! INITIALIZE AUXILIARY SIMULATION
  nsteps_aux_quick = param_library%get(WIGAUX//"/nsteps_quick")
  nsteps_aux_quick_therm = param_library%get(WIGAUX//"/nsteps_quick_therm")
  nsteps_aux_precise = param_library%get(WIGAUX//"/nsteps_precise",default=nsteps_aux_quick)  
  nsteps_aux_precise_therm = param_library%get(WIGAUX//"/nsteps_precise_therm",default=nsteps_aux_quick_therm)
  if(nsteps_aux_precise <= nsteps_aux_quick) sample_momentum_precise=.FALSE.

  num_aux_sim = param_library%get(WIGAUX//"/num_aux_sim")
  if(num_aux_sim>1) write(*,*) int_to_str(num_aux_sim)//" parallel auxiliary simulations"
  call initialize_auxiliary_simulation(num_aux_sim,auxsim)


  !PENALTY CORRECTION PARAMETERS
  penalty_correction = param_library%get("sampler/penalty_correction",default=.FALSE.)
  if(penalty_correction) then
    if(num_aux_sim<4) STOP "Error: cannot perform penalty correction if num_aux_sim<4"
    write(*,*) "penalty correction for momentum sampling"
    nsteps_penalty_btwn_samples = param_library%get("sampler/nsteps_penalty_btwn_samples")
    nsteps_penalty_therm = param_library%get("sampler/nsteps_penalty_therm")
    accratio_penalty_avg = 0._dp
    eta_penalty_avg = 0._dp
    eta_too_big_ratio_avg = 0._dp
  endif

  

  ! WARM UP AUXILIARY SIMULATION
  auxsim%nsteps = nsteps_aux_precise
  auxsim%nsteps_therm = nsteps_aux_precise_therm
  call compute_wigner_forces(X,auxsim)
  auxsim%nsteps = nsteps_aux_quick
  auxsim%nsteps_therm = nsteps_aux_quick_therm
  
  !----------------------------------------
  call open_output_files(param_library)
  !----------------------------------------
  ! SIMULATION LOGIC GOES HERE

  open(newunit=ux,file=output%working_directory//"sample_position")
  open(newunit=up,file=output%working_directory//"sample_momentum")
  open(newunit=uw,file=output%working_directory//"sample_weights")
  open(newunit=uk2,file=output%working_directory//"sample_k2")
  if(num_aux_sim > 1) then
    open(newunit=uk2var,file=output%working_directory//"sample_k2_stddev")
  endif
  verbose=.FALSE.

  !MAIN SAMPLE LOOP
  DO isample = 1,nsamples
    !START TIMERS
    call full_step_timer%start()
    call cpu_time(step_cpu_time_start)
    avg_counter = avg_counter+1
    
    ! DETERMINE IF WE SHOULD PRINT SOME INFO
    if(mod(isample,write_stride)==0) then
      write(*,*)
      write(*,*) "SAMPLE "//int_to_str(isample)//" / "//int_to_str(nsamples)
      verbose = .TRUE.
    else
      verbose = .FALSE.
    endif

    !-------------------------------
    ! POSITION SAMPLING (LANGEVIN PROPAGATION)
    call step_timer%start()
    call sampler_propagation(X,P,nsteps_btwn_samples,auxsim,verbose)  
    lgv_time_avg =  lgv_time_avg  &
      + (step_timer%elapsed_time() - lgv_time_avg)/avg_counter
    !-------------------------------

    !-------------------------------
    ! MOMENTUM SAMPLING
    call step_timer%start()
    call sample_Pw(verbose)
    momentum_time_avg =  momentum_time_avg  &
      + (step_timer%elapsed_time() - momentum_time_avg)/avg_counter
    !-------------------------------
    
    !-------------------------------
    ! CLASSICAL DYNAMICS
    call step_timer%start()
    !$OMP PARALLEL DO PRIVATE(Qekin)
    DO ip=1,n_momenta_per_sample
        call compute_kinetic_energy(Pw(:,:,ip),Qekin,Qtemp(ip))
        if(charges_provided) then
          call classical_dynamics(X,Pw(:,:,ip),nsteps_dyn,dt_dyn,Cpp(:,:,ip),Cmumu(:,:,ip))
        else
          call classical_dynamics(X,Pw(:,:,ip),nsteps_dyn,dt_dyn,Cpp(:,:,ip))
        endif
    ENDDO
    !$OMP END PARALLEL DO
    dynamics_time_avg =  dynamics_time_avg  &
      + (step_timer%elapsed_time() - dynamics_time_avg)/avg_counter
    !-------------------------------

    ! WINDOW AVERAGES
    Qtemp_avg = Qtemp_avg + (kinWeight*SUM(Qtemp)/n_momenta_per_sample - Qtemp_avg)/avg_counter
    pot_avg = pot_avg + (kinWeight*Pot(X) - pot_avg)/avg_counter

    ! GLOBAL AVERAGES
    Cpp_avg = Cpp_avg + (kinWeight*SUM(Cpp,dim=3)/n_momenta_per_sample - Cpp_avg)/isample
    if(charges_provided) then
      Cmumu_avg = Cmumu_avg + (kinWeight*SUM(Cmumu,dim=3)/n_momenta_per_sample - Cmumu_avg)/isample
    endif
    kinWeight_avg = kinWeight_avg + (kinWeight - kinWeight_avg)/isample

    ! WRITE SAMPLES
    do ip = 1,n_momenta_per_sample
      write(ux,*) X(:,:)
      write(up,*) Pw(:,:,ip)
      write(uw,*) kinWeight
      write(uk2,*) auxsim%k2(:,:)
      if(num_aux_sim > 1) then
        write(uk2var,*) sqrt(auxsim%k2var(:,:))
      endif
    enddo

    step_time_avg =  step_time_avg  &
      + (full_step_timer%elapsed_time() - step_time_avg)/avg_counter
    
    call cpu_time(step_cpu_time_finish)
    step_cpu_time_avg = step_cpu_time_avg &
      + (step_cpu_time_finish-step_cpu_time_start - step_cpu_time_avg)/avg_counter

    if(verbose) then
      ! PRINT TIMINGS
      write(*,*) "quantum_effective_temperature=",Qtemp_avg/kinWeight_avg,"K"
      write(*,*) "average_potential_energy=",pot_avg/kinWeight_avg*au%kcalpermol,"kcal/mol"
      write(*,*) "average_kinWeight=",kinWeight_avg
      write(*,*) "AVERAGE TIMINGS PER SAMPLE:"
      write(*,*) "  position sampling :",lgv_time_avg, "s. (for "//int_to_str(nsteps_btwn_samples)//" steps of lgv)"
      write(*,*) "  momentum sampling :",momentum_time_avg,"s."
      write(*,*) "  classical propagation :",dynamics_time_avg,"s."
      job_time = step_time_avg-(lgv_time_avg+momentum_time_avg+dynamics_time_avg)
      write(*,*) "  I/O overhead :",job_time,"s."
      write(*,*) "  total time: ",step_time_avg,"s. || CPU time :",step_cpu_time_avg,"s."

      job_time = step_time_avg*nsamples
      remaining_time = step_time_avg*(nsamples-isample)
      write(*,*) "estimated job time : "//sec2human(job_time)
      write(*,*) "estimated remaining time : "//sec2human(remaining_time)

      ! RESET WINDOW AVERAGES
      avg_counter=0
      dynamics_time_avg=0._dp
      lgv_time_avg = 0._dp
      momentum_time_avg = 0._dp

      ! WRITE INTERMEDIATE RESULTS
      call write_ivr_results(Cpp_avg/kinWeight_avg,nsteps_dyn,dt_dyn,gamma_dyn,spectrum_cutoff,"Cpp")
      call compute_mCvv(Cpp_avg/kinWeight_avg,mCvv_avg,nsteps_dyn)
      call write_ivr_results(mCvv_avg,nsteps_dyn,dt_dyn,gamma_dyn,spectrum_cutoff,"mCvv")
      if(charges_provided) then
        call write_ivr_results(Cmumu_avg/kinWeight_avg,nsteps_dyn,dt_dyn,gamma_dyn,spectrum_cutoff,"Cmumu")
      endif

    endif
  ENDDO

  ! WRITE FINAL RESULTS
  call write_ivr_results(Cpp_avg/kinWeight_avg,nsteps_dyn,dt_dyn,gamma_dyn,spectrum_cutoff,"Cpp")
  call compute_mCvv(Cpp_avg/kinWeight_avg,mCvv_avg,nsteps_dyn)
  call write_ivr_results(mCvv_avg,nsteps_dyn,dt_dyn,gamma_dyn,spectrum_cutoff,"mCvv")
  if(charges_provided) then
    call write_ivr_results(Cmumu_avg/kinWeight_avg,nsteps_dyn,dt_dyn,gamma_dyn,spectrum_cutoff,"Cmumu")
  endif

  close(ux)
  close(up)
  close(uw)
  close(uk2)
  close(uk2var)

  call cpu_time(cpu_time_finish)
  write(*,*)
  write(*,*) "JOB DONE in "//sec2human(full_timer%elapsed_time())
  write(*,*) "( CPU TIME = "//sec2human(cpu_time_finish-cpu_time_start)//" )"

CONTAINS

  subroutine sample_Pw(verbose)
    implicit none
    logical, intent(in) :: verbose

    ! COMPUTE PRECISE k2 IF NECESSARY
    if(sample_momentum_precise) then
      auxsim%nsteps = nsteps_aux_precise
      auxsim%nsteps_therm = nsteps_aux_precise_therm
      call compute_wigner_forces(X,auxsim)
      auxsim%nsteps = nsteps_aux_quick
      auxsim%nsteps_therm = nsteps_aux_quick_therm
    endif

    if(penalty_correction) then
      ! SAMPLE MOMENTUM WITH PENALTY CORRECTION
      call sample_wigner_momentum_penalty(Pw,kinWeight,auxsim, &
              nsteps_penalty_therm,nsteps_penalty_btwn_samples)
      
      accratio_penalty_avg = accratio_penalty_avg  &
          + (auxsim%accratio_penalty - accratio_penalty_avg)/avg_counter
      eta_penalty_avg = eta_penalty_avg  &
          + (auxsim%eta_penalty - eta_penalty_avg)/avg_counter
      eta_too_big_ratio_avg = eta_too_big_ratio_avg  &
          + (auxsim%eta_too_big_ratio - eta_too_big_ratio_avg)/avg_counter

    else
      ! SAMPLE MOMENTUM WITH MEAN k2
      call sample_wigner_momentum_standard(Pw,kinWeight,auxsim)
    endif    
    
    if(verbose) then
      if(penalty_correction) then
        ! PRINT PENALTY INFO
        write(*,*) "penalty_accratio=", real(accratio_penalty_avg,sp)
        write(*,*) "average_penalty_eta_(=xi^2/n)=", real(eta_penalty_avg,sp)
        write(*,*) "ratio_of_eta>0.25=", real(eta_too_big_ratio_avg,sp)
      endif
    endif

  end subroutine sample_Pw

  subroutine classical_dynamics(X0,P0,nsteps,dt,Cpp,Cmumu)
    implicit none
    real(dp), intent(in) :: X0(:,:),P0(:,:)
    integer, intent(in) :: nsteps
    real(dp), intent(in) :: dt
    real(dp), intent(inout) :: Cpp(:,:)
    real(dp), intent(inout), OPTIONAL ::  Cmumu(:,:)
    real(dp), allocatable :: X(:,:),P(:,:),F(:,:)
    real(dp) :: dt2
    real(dp) :: mu0(ndim),mu(ndim)
    integer :: istep,j 

    Cpp(:,:) = 0._dp    
    Cpp(:,1) = RESHAPE(P0*P0, (/ndof/))

    if(present(Cmumu)) then
      Cmumu(:,:) = 0._dp   
      call get_dipole(P0,mu0) 
      Cmumu(:,1) = mu0*mu0
    endif

    allocate(X(nat,ndim),P(nat,ndim),F(nat,ndim))
    X(:,:)=X0(:,:) ; P(:,:)=P0(:,:)
    F = -dPot(X)
    dt2=0.5_dp*dt
    
    do istep = 2,nsteps
      ! VELOCITY VERLET PROPAGATION
      P = P + dt2*F
      do j=1,ndim
        X(:,j) = X(:,j) + dt*P(:,j)/mass(:)
      enddo
      F = -dPot(X)
      P = P + dt2*F

      !UPDATE Cpp
      Cpp(:,istep) = RESHAPE(P0*P, (/ndof/))
      if(present(Cmumu)) then
        call get_dipole(P,mu) 
        Cmumu(:,istep) = mu0*mu
      endif
    enddo

    deallocate(X,P,F)

  end subroutine classical_dynamics

  subroutine write_ivr_results(Cpp,nsteps,dt,gamma,cutoff,prefix)
    implicit none
    real(dp), intent(in) :: Cpp(:,:), dt
    integer, intent(in) :: nsteps
    real(dp), intent(in) :: gamma, cutoff
    character(*),intent(in) :: prefix
    integer :: istep
    real(dp) :: domega,t,u0
    real(dp), allocatable :: spectrum(:,:),Cppdamp(:,:)
    integer :: ncut_spectrum

    ! WRITE RAW Cpp AND mCvv
    call write_Cpp_array(Cpp,nsteps,dt*au%fs,name=prefix//"_ivr.out")   

    allocate(spectrum(size(Cpp,1),nsteps))
    ! COMPUTE AND WRITE RAW SPECTRUM
    call compute_spectrum(Cpp,nsteps,dt,spectrum,domega)
    if(cutoff>0) then
      ncut_spectrum = nint(cutoff/domega)
    else
      ncut_spectrum = nsteps
    endif    
    call write_Cpp_array(spectrum,ncut_spectrum,domega*au%cm1,name=prefix//"_spectrum_raw.out")

    ! KUBO TRANSFORM
    DO istep=2,nsteps
      u0=0.5_dp*(istep-1)*domega/kT
      spectrum(:,istep) = spectrum(:,istep)*(tanh(u0)/u0)
    enddo    
    call write_Cpp_array(spectrum,ncut_spectrum,domega*au%cm1,name=prefix//"_spectrum_raw_kubo.out")

    if(gamma>0) then
      ! WRITE DATA WITH GAUSSIAN TIME WINDOW SMOOTHING
      allocate(Cppdamp(size(Cpp,1),nsteps))
      do istep=1,nsteps
        t=(istep-1)*dt
        Cppdamp(:,istep) = Cpp(:,istep)*exp(-0.5_dp*(t*gamma)**2)
      enddo
      call write_Cpp_array(Cppdamp,nsteps,dt*au%fs,name=prefix//"_ivr_damp.out")   
      call compute_spectrum(Cppdamp,nsteps,dt,spectrum,domega)
      call write_Cpp_array(spectrum,ncut_spectrum,domega*au%cm1,name=prefix//"_spectrum_damp.out")
      ! KUBO TRANSFORM
      DO istep=2,nsteps
        u0=0.5_dp*(istep-1)*domega/kT
        spectrum(:,istep) = spectrum(:,istep)*(tanh(u0)/u0)
      enddo
      call write_Cpp_array(spectrum,ncut_spectrum,domega*au%cm1,name=prefix//"_spectrum_damp_kubo.out")

      deallocate(Cppdamp)
    endif

    deallocate(spectrum)

  end subroutine write_ivr_results

  subroutine write_Cpp_array(Cpp,nsteps,dx,name)
    implicit none
    real(dp), intent(in) :: Cpp(:,:), dx
    character(*), intent(in) :: name
    integer, intent(in) :: nsteps
    integer :: istep,uf

    open(newunit=uf,file=output%working_directory//name)
    do istep = 1,nsteps
      write(uf,*) (istep-1)*dx , Cpp(:,istep)
    enddo
    close(uf)

  end subroutine write_Cpp_array

  subroutine compute_mCvv(Cpp,mCvv,nsteps)
    implicit none
    real(dp), intent(in) :: Cpp(:,:)
    integer, intent(in) :: nsteps
    real(dp), intent(inout) :: mCvv(:,:)
    integer :: istep,j,i,c
    
    mCvv(:,:) = 0._dp
    do istep = 1,nsteps   
      c=0   
      do j=1,ndim; do i=1,nat
        c=c+1
        mCvv(j,istep) = mCvv(j,istep) + Cpp(c,istep)/(2*mass(i))
      enddo; enddo
    enddo

  end subroutine compute_mCvv

  subroutine compute_spectrum(Cpp,nsteps,dt,spectrum,domega)
    real(dp), intent(in) :: Cpp(:,:), dt
    integer, intent(in) :: nsteps
    real(dp), intent(inout) :: spectrum(:,:)
    real(dp), intent(out) :: domega
    real(dp), allocatable :: fr(:),fi(:),work(:)
    integer :: ifail,i,k
    real(dp) :: norm

    norm = dt*sqrt(real(2*nsteps))
    allocate(fr(2*nsteps),fi(2*nsteps),work(2*nsteps))

    do i=1,size(Cpp,1)
      fi(:)=0._dp
      work(:)=0._dp      
      fr(1:nsteps) = Cpp(i,:)
      fr(nsteps+1)=fr(nsteps) ! SYMMETRIZE Cpp
      do k=1,nsteps-1
        fr(nsteps+k+1)=fr(nsteps-k+1)
      enddo
      ifail=0
      call C06FCF(fr,fi,2*nsteps,work,ifail) ! FFT
      spectrum(i,:) = fr(1:nsteps)*norm
    enddo
    domega = 2._dp*pi/(dt*(2*nsteps))

    deallocate(fr)
    deallocate(fi)
    deallocate(work)
    
  end subroutine compute_spectrum

END PROGRAM WiSE
