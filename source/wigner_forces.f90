module wigner_forces
  use kinds
  use nested_dictionaries
  use system_commons
  use timer_module
  use random
  use potential
  use matrix_operations
  implicit none

  PRIVATE
  PUBLIC :: WIGAUX &
            ,initialize_wigner_commons &
            ,initialize_auxiliary_simulation &
            ,compute_wigner_forces &
            ,WIG_AUX_SIM_TYPE

  integer, save :: n_threads

  CHARACTER(*), PARAMETER :: WIGAUX = "auxiliary"

  real(dp), save :: dt
  INTEGER, save :: nu !number of beads
  real(dp), save :: dBeta

  real(dp), allocatable, save :: sqrt_mass(:)
  real(dp), save :: delta_mass_factor, sqrt_delta_mass_factor  

  ! POLYMER PARAMETERS
  real(dp), allocatable, save :: EigMat(:,:)
  real(dp), allocatable, save :: EigMatTr(:,:)
  real(dp), allocatable, save :: OmK(:)
  real(dp), allocatable, save :: mOmK(:,:)

  !PIOUD PARAMETERS
  ! real(dp), allocatable :: mu(:,:,:)
  real(dp), allocatable, save :: OUsig(:,:,:,:)
  real(dp), allocatable, save :: expOmk(:,:,:,:)

  ! BAOAB PARAMETERS
  real(dp), allocatable, save :: gammaExp(:,:)
  real(dp), allocatable, save :: sigmaBAOAB(:,:)
  LOGICAL, save :: BAOAB_num

  TYPE WIG_POLYMER_TYPE
    real(dp), allocatable :: X(:,:,:)
    real(dp), allocatable :: EigX(:,:,:)
    real(dp), allocatable :: EigV(:,:,:)

    real(dp), allocatable :: F_beads(:,:,:)
    real(dp), allocatable :: Pot_beads(:)

    real(dp), allocatable :: Delta_packed(:)

    real(dp), allocatable :: k2(:,:)
    real(dp), allocatable :: F(:,:)

  END TYPE WIG_POLYMER_TYPE

  TYPE WORK_VARIABLES
      real(dp), allocatable :: F(:,:)
      real(dp), allocatable :: EigX(:),EigV(:)
      real(dp), allocatable :: R(:), R1(:), R2(:)
      real(dp), allocatable :: k2var(:,:),Fvar(:)
  END TYPE WORK_VARIABLES

  TYPE WIG_AUX_SIM_TYPE
    INTEGER :: NUM_AUX_SIM
    INTEGER :: nsteps, nsteps_therm

    real(dp), allocatable :: Fw(:,:)
    real(dp), allocatable :: k2(:,:)
    real(dp), allocatable :: Fcl(:,:)
    real(dp), allocatable :: k2xi2(:,:)
    real(dp), allocatable :: k2var(:,:)
    real(dp), allocatable :: Fvar(:,:)
    real(dp) :: Potcl

    real :: computation_time
    real(dp) :: accratio_penalty
    real(dp) :: eta_penalty
    real(dp) :: eta_too_big_ratio

    TYPE(WIG_POLYMER_TYPE), DIMENSION(:), allocatable :: polymers
  END TYPE WIG_AUX_SIM_TYPE  

  TYPE(WORK_VARIABLES), DIMENSION(:), allocatable, save :: WORKS    

CONTAINS

  subroutine compute_wigner_forces(X,auxsim)
    !$ USE OMP_LIB
    IMPLICIT NONE
		REAL(dp), intent(in) :: X(:,:)
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    INTEGER :: i,j,k,INFO,i_thread,c,u
    TYPE(timer_type) :: timer_full, timer_elem
    real(dp), allocatable :: k2tmp(:,:)

    if(ANY(ISNAN(X))) STOP "ERROR: system diverged!"

    call timer_full%start()



    ! GET CLASSICAL FORCE     
    call get_pot_info(X,auxsim%Potcl,auxsim%Fcl)        

    i_thread=1
    !$OMP PARALLEL DO LASTPRIVATE(i_thread)    
    DO i=1,auxsim%NUM_AUX_SIM
        !$ i_thread = OMP_GET_THREAD_NUM()+1         
        call sample_forces(X,auxsim%nsteps,auxsim%nsteps_therm,auxsim%polymers(i),WORKS(i_thread))
    ENDDO
    !$OMP END PARALLEL DO

    if(auxsim%NUM_AUX_SIM>1) then
      auxsim%k2(:,:)=0._dp
      auxsim%Fw(:,:)=0._dp
      DO i=1,auxsim%NUM_AUX_SIM
        auxsim%k2(:,:)=auxsim%k2(:,:) + auxsim%polymers(i)%k2(:,:)
        auxsim%Fw(:,:)=auxsim%Fw(:,:) + auxsim%polymers(i)%F(:,:)
      ENDDO
      auxsim%k2(:,:)=auxsim%k2(:,:)/auxsim%NUM_AUX_SIM
      auxsim%Fw(:,:)=auxsim%Fw(:,:)/auxsim%NUM_AUX_SIM

      allocate(k2tmp(ndof,ndof))
      auxsim%k2var(:,:)=0._dp 
      auxsim%k2xi2(:,:)=0._dp 
      auxsim%Fvar(:,:)=0._dp 
      DO i=1,auxsim%NUM_AUX_SIM
        k2tmp(:,:) = auxsim%polymers(i)%k2(:,:) - auxsim%k2(:,:)
        auxsim%k2var(:,:) = auxsim%k2var +  k2tmp(:,:)**2
        auxsim%k2xi2(:,:)=auxsim%k2xi2(:,:) + matmul(k2tmp,k2tmp)
        auxsim%Fvar(:,:)=auxsim%Fvar(:,:) + &
            (auxsim%polymers(i)%F(:,:) - auxsim%Fw(:,:))**2
      ENDDO
      auxsim%k2var = auxsim%k2var/REAL(auxsim%NUM_AUX_SIM*(auxsim%NUM_AUX_SIM-1),dp)
      auxsim%k2xi2 = auxsim%k2xi2/REAL(auxsim%NUM_AUX_SIM*(auxsim%NUM_AUX_SIM-1),dp)
      auxsim%Fvar = auxsim%Fvar/REAL(auxsim%NUM_AUX_SIM*(auxsim%NUM_AUX_SIM-1),dp)
      deallocate(k2tmp)
    else
      auxsim%k2(:,:) = auxsim%polymers(1)%k2(:,:)
      auxsim%Fw(:,:) = auxsim%polymers(1)%F(:,:)
    endif
    
    auxsim%computation_time = timer_full%elapsed_time()

  end subroutine compute_wigner_forces

  subroutine sample_forces(X,nsteps,nsteps_therm,polymer,WORK)
    IMPLICIT NONE
		real(dp), intent(in) :: X(:,:)
    integer, intent(in) :: nsteps,nsteps_therm
    CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
    CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
    INTEGER :: m,n_dof,ios
    INTEGER :: i,j,k,l,c
    LOGICAL :: move_accepted


    polymer%k2(:,:) = 0._dp
    polymer%F(:,:) = 0._dp

    !THERMALIZATION
    DO m=1,nsteps_therm
        CALL PIOUD_step(X,polymer,WORK)
    ENDDO

    !PRODUCTION
    DO m=1,nsteps

        !GENERATE A NEW CONFIG
        CALL PIOUD_step(X,polymer,WORK)

        WORK%F=SUM(polymer%F_beads(:,:,:)/real(nu,dp),dim=1)

        polymer%Delta_packed=RESHAPE(2._dp*polymer%X(nu,:,:),(/ndof/))
        !COMPUTE ONLY THE LOWER TRIANGULAR PART
        do j=1,ndof ; do i=j,ndof
          polymer%k2(i,j)=polymer%k2(i,j)  &
            + (polymer%Delta_packed(i)*polymer%Delta_packed(j) &
                  -polymer%k2(i,j))/REAL(m,dp)
        enddo ; enddo

        !UPDATE MEAN VALUES
		    polymer%F=polymer%F + (WORK%F-polymer%F)/REAL(m,dp)   

    ENDDO

    !SYMMETRIZE k2 (FILL THE UPPER TRIANGULAR PART)
    DO j=2,ndof ; DO i=1,j-1
        polymer%k2(i,j)=polymer%k2(j,i)
    ENDDO ; ENDDO

  end subroutine sample_forces

!-----------------------------------------------------------------
! INITIALIZATIONS

  subroutine initialize_wigner_commons(param_library)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    integer :: i

    nu = param_library%get(WIGAUX//"/nbeads")
    !! @input_file auxiliary/n_beads
    dt = param_library%get(WIGAUX//"/dt")
    dBeta=beta/REAL(nu,dp)

    call get_polymer_masses(param_library)
    call initialize_PIOUD()

    ! GET NUMBER OF THREADS        
    n_threads=1
    !$ n_threads=param_library%get("PARAMETERS/omp_num_threads",default=1)

    !INITIALIZE WORK VARIABLES
    allocate(WORKS(n_threads))
    DO i=1,n_threads
        allocate( &
            WORKS(i)%F(nat,ndim), &
            WORKS(i)%R(nat), &
            WORKS(i)%R1(nu), &
            WORKS(i)%R2(nu) &
        )
    ENDDO

  end subroutine initialize_wigner_commons

  subroutine initialize_auxiliary_simulation(n_auxsim,auxsim)
    integer, intent(in) :: n_auxsim
    TYPE(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    integer :: i,ibead 

    call deallocate_auxiliary_simulation(auxsim)

    auxsim%NUM_AUX_SIM = n_auxsim

    allocate(auxsim%Fcl(nat,ndim)) ; auxsim%Fcl = 0._dp
    allocate(auxsim%Fw(nat,ndim)) ; auxsim%Fw = 0._dp
    allocate(auxsim%k2(ndof,ndof)) ; auxsim%k2 = 0._dp
    if(n_auxsim>1) then
      allocate(auxsim%k2var(ndof,ndof))
      allocate(auxsim%k2xi2(ndof,ndof))
      allocate(auxsim%Fvar(nat,ndim))
    endif

    allocate(auxsim%polymers(n_auxsim))
    DO i=1,auxsim%NUM_AUX_SIM
        allocate( &
            auxsim%polymers(i)%X(nu,nat,ndim), &
            auxsim%polymers(i)%EigV(nu,nat,ndim), &
            auxsim%polymers(i)%EigX(nu,nat,ndim), &
            auxsim%polymers(i)%F_beads(0:nu,nat,ndim), &
            auxsim%polymers(i)%Pot_beads(0:nu), &
            auxsim%polymers(i)%Delta_packed(ndof), &
            auxsim%polymers(i)%k2(ndof,ndof), &
            auxsim%polymers(i)%F(nat,ndim) &
        )
        auxsim%polymers(i)%Delta_packed=0._dp
        auxsim%polymers(i)%X=0._dp
        auxsim%polymers(i)%EigX=0._dp
        call sample_momenta(auxsim%polymers(i))
        ! polymers(i)%P_prev=polymers(i)%P
        ! polymers(i)%X_prev=polymers(i)%X    
    ENDDO


  end subroutine initialize_auxiliary_simulation

  subroutine get_polymer_masses(param_library)
    IMPLICIT NONE
    TYPE(DICT_STRUCT), intent(in) :: param_library
    TYPE(DICT_STRUCT), POINTER :: m_lib
    CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
    LOGICAL, ALLOCATABLE :: is_sub(:)
    REAL(dp) :: m_tmp
    INTEGER :: i
    CHARACTER(2) :: symbol

    ALLOCATE(sqrt_mass(nat))
    
    if(.not. param_library%has_key(WIGAUX//"/auxiliary_masses")) then
        sqrt_mass=SQRT(mass)
        return
    endif

    m_lib => param_library%get_child(WIGAUX//"/auxiliary_masses")
    !! @input_file auxiliary/auxiliary_masses (sub dict)
    write(*,*)
    write(*,*) "g-WiLD auxiliary masses:"
    call dict_list_of_keys(m_lib,keys,is_sub)
    DO i=1,size(keys)
      if(is_sub(i)) CYCLE
      m_tmp=m_lib%get(keys(i))
      symbol=trim(keys(i))
      symbol(1:1)=to_upper_case(symbol(1:1))
      write(*,*) symbol," : ", real(m_tmp/au%Mprot),"amu"
    ENDDO
      write(*,*)
      do i=1, nat
          sqrt_mass(i)=m_lib%get(trim(element_symbols(i)) &
                                          ,default=mass(i) )
          !if(sqrt_mass(i) /= system%mass(i)) write(*,*) "mass",i,"modified to", sqrt_mass(i)/Mprot," amu"                    
    enddo
    sqrt_mass=sqrt(sqrt_mass)

  end subroutine get_polymer_masses


  subroutine initialize_PIOUD()
    IMPLICIT NONE
    INTEGER :: i,j,INFO
    real(dp), allocatable ::thetaInv(:,:,:,:),OUsig2(:,:,:,:),TMP(:)
    real(dp) :: Id(2,2)

    allocate( &
        EigMat(nu,nu), &
        EigMatTr(nu,nu), &
        OmK(nu), &
        mOmK(nu,nat), &
        gammaExp(nu,nat), &
        sigmaBAOAB(nu,nat) &
    )

    !INITIALIZE DYNAMICAL MATRIX
    EigMat=0
    do i=1,nu-1
        EigMat(i,i)=2
        EigMat(i+1,i)=-1
        EigMat(i,i+1)=-1
    enddo
    EigMat(nu,nu)=2
    EigMat(1,nu)=-1
    EigMat(nu,1)=-1
    EigMat(nu-1,nu)=1
    EigMat(nu,nu-1)=1

    !SOLVE EIGENPROBLEM
    allocate(TMP(3*nu-1))
    CALL DSYEV('V','L',nu,EigMat,nu,Omk,TMP,3*nu-1,INFO)
    if(INFO/=0) then
        write(0,*) "Error during computation of eigenvalues for EigMat. code:",INFO
        stop 
    endif
    deallocate(TMP)
    EigMatTr=transpose(EigMat)
    Omk=SQRT(Omk)/dBeta
    DO i=1,nat
        mOmk(:,i)=Omk*sqrt(mass(i))/sqrt_mass(i) 
    ENDDO
    write(*,*)
    write(*,*) "INTERNAL FREQUENCIES OF THE CHAIN"
    do i=1,nu
        write(*,*) i, Omk(i)*au%THz/(2*pi),"THz"
    enddo
    write(*,*)  
        

    allocate(expOmk(nu,2,2,nat))  

    Id=0
    Id(1,1)=1
    Id(2,2)=1

    allocate( &
        thetaInv(nu,2,2,nat), &
        OUsig2(nu,2,2,nat) &
    )
    if(allocated(OUsig)) deallocate(OUsig)
    allocate(OUsig(nu,2,2,nat)) 
    if(allocated(expOmk)) deallocate(expOmk)
    allocate(expOmk(nu,2,2,nat))              

    DO i=1,nat
        expOmk(:,1,1,i)=exp(-mOmk(:,i)*dt) &
                                *(1-mOmk(:,i)*dt)
        expOmk(:,1,2,i)=exp(-mOmk(:,i)*dt) &
                                *(-dt*mOmk(:,i)**2)
        expOmk(:,2,1,i)=exp(-mOmk(:,i)*dt)*dt
        expOmk(:,2,2,i)=exp(-mOmk(:,i)*dt) &
                                *(1+mOmk(:,i)*dt)

        thetaInv(:,1,1,i)=0._dp
        thetaInv(:,1,2,i)=-1._dp
        thetaInv(:,2,1,i)=1._dp/mOmk(:,i)**2
        thetaInv(:,2,2,i)=2._dp/mOmk(:,i)
    ENDDO

    !COMPUTE COVARIANCE MATRIX
    DO i=1,nat
        OUsig2(:,1,1,i)=(1._dp- (1._dp-2._dp*dt*mOmk(:,i) &
                            +2._dp*(dt*mOmk(:,i))**2 &
                        )*exp(-2._dp*mOmk(:,i)*dt) &
                        )/dBeta
        OUsig2(:,2,2,i)=(1._dp- (1._dp+2._dp*dt*mOmk(:,i) &
                            +2._dp*(dt*mOmk(:,i))**2 &
                        )*exp(-2._dp*mOmk(:,i)*dt) &
                        )/(dBeta*mOmk(:,i)**2)
        OUsig2(:,2,1,i)=2._dp*mOmk(:,i)*(dt**2) &
                        *exp(-2._dp*mOmk(:,i)*dt)/dBeta
        OUsig2(:,1,2,i)=OUsig2(:,2,1,i)

        !COMPUTE CHOLESKY DECOMPOSITION
        OUsig(:,1,1,i)=sqrt(OUsig2(:,1,1,i))
        OUsig(:,1,2,i)=0
        OUsig(:,2,1,i)=OUsig2(:,2,1,i)/OUsig(:,1,1,i)
        OUsig(:,2,2,i)=sqrt(OUsig2(:,2,2,i)-OUsig(:,2,1,i)**2)
    ENDDO

    !CHECK CHOLESKY DECOMPOSITION
    ! write(*,*) "sum of squared errors on cholesky decomposition:"
    ! do i=1,nu
    ! 	write(*,*) i, sum( (matmul(OUsig(i,:,:),transpose(OUsig(i,:,:)))-OUsig2(i,:,:))**2 )
    ! enddo

  end subroutine initialize_PIOUD

!-------------------------------------------------------------------

  subroutine deallocate_auxiliary_simulation(auxsim)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    integer :: i

    if(allocated(auxsim%polymers)) then
      do i = 1, size(auxsim%polymers)
        call deallocate_polymer(auxsim%polymers(i))
      enddo
      deallocate(auxsim%polymers)
    endif

  end subroutine deallocate_auxiliary_simulation

  subroutine deallocate_polymer(polymer)
    implicit none
    type(WIG_POLYMER_TYPE), intent(inout) :: polymer

    if(allocated(polymer%X)) deallocate(polymer%X)
    if(allocated(polymer%EigV)) deallocate(polymer%EigV)
    if(allocated(polymer%EigX)) deallocate(polymer%EigX)
    if(allocated(polymer%F_beads)) deallocate(polymer%F_beads)
    if(allocated(polymer%Pot_beads)) deallocate(polymer%Pot_beads)
    if(allocated(polymer%Delta_packed)) deallocate(polymer%Delta_packed)
    if(allocated(polymer%k2)) deallocate(polymer%k2)
    if(allocated(polymer%F)) deallocate(polymer%F)

    end subroutine deallocate_polymer

  subroutine sample_momenta(polymer)
    IMPLICIT NONE
    CLASS(WIG_POLYMER_TYPE), INTENT(inout) :: polymer
    INTEGER :: i,j,k

    DO k=1,ndim ; DO j=1,nat          
        call randGaussN(polymer%EigV(:,j,k))
        polymer%EigV(:,j,k)=polymer%EigV(:,j,k)/sqrt(dBeta)   !*sqrt_mass(j)    
    ENDDO  ; ENDDO
  end subroutine sample_momenta

  subroutine update_beads_forces(q,polymer)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(dp), INTENT(in) :: q(:,:)
		INTEGER :: i
    real(dp), allocatable :: F(:,:)

    allocate(F(nat,ndim))
        
		do i=1,nu-1
      call get_pot_info(q+polymer%X(i,:,:),polymer%Pot_beads(i),F)
      polymer%F_beads(i,:,:) = F(:,:)
		enddo
    call get_pot_info(q+polymer%X(nu,:,:),polymer%Pot_beads(0),F)
    polymer%F_beads(0,:,:)=0.5_dp*F(:,:)

    call get_pot_info(q-polymer%X(nu,:,:),polymer%Pot_beads(nu),F)
    polymer%F_beads(nu,:,:)=0.5_dp*F(:,:)

    deallocate(F)
    polymer%Pot_beads(0)=0.5_dp*polymer%Pot_beads(0)
    polymer%Pot_beads(nu)=0.5_dp*polymer%Pot_beads(nu)

	end subroutine update_beads_forces

  subroutine apply_forces(polymer,tau)
		implicit none
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
		REAL(dp), INTENT(in) :: tau
		INTEGER :: i,j
    REAL(dp) :: EigF(nu),F(nu)

    do j=1,ndim ; DO i=1,nat
        !COMPUTE FORCE FOR NORMAL MODES
        F(1:nu-1)=polymer%F_beads(1:nu-1,i,j)/sqrt_mass(i)
        F(nu)=(polymer%F_beads(0,i,j)-polymer%F_beads(nu,i,j))/sqrt_mass(i)
        call DGEMV('N',nu,nu,1._8,EigMatTr,nu,F,1,0._8,EigF,1)
        !EigF=matmul(EigMatTr,F)

        polymer%EigV(:,i,j)=polymer%EigV(:,i,j)+tau*EigF(:)
    ENDDO ; ENDDO

	end subroutine apply_forces

  subroutine PIOUD_step(X,polymer,WORK)
		IMPLICIT NONE
    real(dp), intent(in) :: X(:,:)
		TYPE(WIG_POLYMER_TYPE), INTENT(INOUT) :: polymer
    CLASS(WORK_VARIABLES), INTENT(inout) :: WORK
		integer :: i,j,k
    REAL(dp) :: EigX0(nu), EigV0(nu)


    !CALL apply_forces(polymer,0.5_dp*dt)

    do j=1,ndim ; do i=1,nat

        !SAVE INITIAL VALUE TEMPORARILY
        EigV0=polymer%EigV(:,i,j)
        EigX0=polymer%EigX(:,i,j)

        !GENERATE RANDOM VECTORS
        CALL RandGaussN(WORK%R1)
        CALL RandGaussN(WORK%R2)

        !PROPAGATE Ornstein-Uhlenbeck WITH NORMAL MODES
        polymer%EigV(:,i,j)=EigV0(:)*expOmk(:,1,1,i) &
            +EigX0(:)*expOmk(:,1,2,i) &
            +OUsig(:,1,1,i)*WORK%R1(:)

        polymer%EigX(:,i,j)=EigV0(:)*expOmk(:,2,1,i) &
            +EigX0(:)*expOmk(:,2,2,i) &
            +OUsig(:,2,1,i)*WORK%R1(:)+OUsig(:,2,2,i)*WORK%R2(:)
        
        
        !TRANSFORM BACK IN COORDINATES
        !polymer%X(:,i,j)=matmul(EigMat,polymer%EigX(:,i,j))/sqrt_mass(i)
        call DGEMV('N',nu,nu,1._8,EigMat,nu,polymer%EigX(:,i,j),1,0._8,polymer%X(:,i,j),1)
        polymer%X(:,i,j)=polymer%X(:,i,j)/sqrt_mass(i)
    enddo ; enddo


    !COMPUTE FORCES	
    CALL update_beads_forces(X,polymer) 

    !APPLY FORCES
    CALL apply_forces(polymer,dt)
    

	end subroutine PIOUD_step

end module wigner_forces
