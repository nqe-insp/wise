MODULE matrix_operations
!! collection of matrix operations subroutines
!!	- wrappers for lapack functions
!!	- fill permutations on a 4th order tensor
!!	- linear interpolation

	USE kinds, only : dp
	IMPLICIT NONE
	real(dp), parameter :: eps=1E-7 ! MAX ERROR ON INVERSE MATRIX
	

	INTERFACE linear_interpolation
		MODULE PROCEDURE :: linear_interpolation_0D
		MODULE PROCEDURE :: linear_interpolation_1D
	END INTERFACE

	contains

	PURE FUNCTION cross_product( vec_1, vec_2 ) RESULT( vec_3 )

    !> @brief Vector product of two three-dimensional Cartesian vectors

    !! Subroutine I/O variables
    REAL( dp ), DIMENSION(3), INTENT( IN )  :: vec_1
    REAL( dp ), DIMENSION(3), INTENT( IN )  :: vec_2
    REAL( dp ), DIMENSION(3)                :: vec_3

    !--------------------------------------------------------------------------

    !! vec_3 = vec_1 X vec_2
    vec_3(1) = vec_1(2) * vec_2(3) - vec_1(3) * vec_2(2)
    vec_3(2) = vec_1(3) * vec_2(1) - vec_1(1) * vec_2(3)
    vec_3(3) = vec_1(1) * vec_2(2) - vec_1(2) * vec_2(1)


  END FUNCTION cross_product

	subroutine sym_mat_diag(mat,EVal,EVec,INFO)
		!! diagonalize a symmetric matrix (which contains at least the lower triange)
		!! @info lapack wrapper (DSYEV)
		implicit none
		real(dp), intent(in) :: mat(:,:)
		integer, intent(out) :: INFO
		real(dp), intent(inout) :: Eval(:),Evec(:,:)
		integer :: Ndim
		real(dp), allocatable :: WORK(:)

		Ndim=size(mat,1)
		if(size(mat,2)/=Ndim) stop "mat must be a square matrix"
		allocate(WORK(3*Ndim))
		EVec=mat
		call DSYEV('V','L',Ndim,EVec,Ndim,EVal,WORK,3*Ndim,INFO)
	!	if(INFO/=0) stop "ERROR DURING MATRIX DIAGONALIZATION."

	end subroutine sym_mat_diag

	function sym_mv(mat,vect) result(mv)
		IMPLICIT NONE
		real(dp), intent(in) :: mat(:,:),vect(:)
		real(dp), allocatable :: mv(:)
		integer :: Ndim,i

		Ndim=size(vect)
		allocate(mv(Ndim))

		call dsymv('L',Ndim,1._dp,mat,Ndim,vect,1,0._dp,mv,1)

	end function sym_mv

	function sym_mat_exp(mat,INFO) result(matExp) !USES LAPACK
		!! compute the exponential of a symmetric matrix (which contains at least the lower triange)
		!! @info lapack wrapper (DSYEV)
		implicit none
		real(dp), intent(in) :: mat(:,:)
		integer, intent(out) :: INFO
		real(dp), allocatable :: matEXp(:,:)
		integer :: Ndim,i
		real(dp), allocatable :: LO(:,:),V(:),WORK(:)

		Ndim=size(mat,1)
		if(size(mat,2)/=Ndim) stop "mat must be a square matrix"
		allocate(matExp(Ndim,Ndim))
		allocate(LO(Ndim,Ndim),WORK(3*Ndim),V(Ndim))

		LO=mat
		call DSYEV('V','L',Ndim,LO,Ndim,V,WORK,3*Ndim,INFO)
		V=exp(V)
		do i=1,Ndim
			matExp(i,:)=V(i)*LO(i,:)
		enddo
		matExp=matmul(transpose(LO),matExp)

	end function sym_mat_exp
	
	function sym_mat_inverse(mat,INFO) result(matInv)
		!! inverse of a symmetric matrix (which contains at least the lower triange)
		!! @info lapack wrapper (DSYSV)
		implicit none
		real(dp), intent(in) :: mat(:,:)
		integer, intent(out) :: INFO
		real(dp), allocatable :: matInv(:,:)
		integer :: Ndim,i
		real(dp), allocatable :: LO(:,:),WORK(:)
		integer, allocatable :: IPIV(:)
		
		Ndim=size(mat,1)
		if(size(mat,2)/=Ndim) stop "mat must be a square matrix"
		allocate(matInv(Ndim,Ndim))
		allocate(LO(Ndim,Ndim),IPIV(Ndim),WORK(3*Ndim))
		
				
		matInv=0
		forall (i=1:Ndim) matInv(i,i)=1
		LO=mat			
		call DSYSV('L',Ndim,Ndim,LO,Ndim,IPIV,matInv,Ndim,WORK,3*Ndim,INFO)
		if(INFO/=0) then
			write(0,*) "ERROR DURING MATRIX INVERSION. ERROR", INFO
			do i=1,Ndim
				write(0,*) mat(i,:)
			enddo
	!		write(0,*) "det=", mat(1,1)*mat(2,2)-mat(2,1)**2
	!		stop
		endif
		!CHECK IF INVERSE IS CORRECT
	!	LO=matmul(mat,matInv)
	!	forall (i=1:Ndim) LO(i,i)=LO(i,i)-1._dp
	!	if(sum(LO**2)>eps) stop "Error on matrix inverse!"		
	end function sym_mat_inverse
	
	function ut3x3_mat_inverse(mat) result(matInv)
		!! inverse a 3x3 upper triangle matrix
		implicit none
		real(dp), intent(in) :: mat(3,3)
		real(dp) :: matInv(3,3)
		integer :: i
		
		matInv=0
		do i=1,3
			matInv(i,i)=1./mat(i,i)
		enddo
		matInv(1,2)=-matInv(1,1)*mat(1,2)*matInv(2,2)
		matInv(2,3)=-matInv(2,2)*mat(2,3)*matInv(3,3)
		matInv(1,3)=-matInv(2,3)*mat(1,2)*matInv(1,1) &
					-matInv(1,1)*mat(1,3)*matInv(3,3)
			
	end function ut3x3_mat_inverse

	subroutine fill_permutations(k4,i1,i2,i3,i4)
	!! fill a 4th order tensor by copying the value
	!! of the input indexes to all their permutations
		implicit none
		real(dp), intent(inout) :: k4(:,:,:,:)
			!! @param fourth order tensor to fill
		integer, intent(in) :: i1,i2,i3,i4
			!! @param indexes of the value to copy
		integer :: perm(0:4),j,k,l
		
		perm(:)=(/ 0,i4,i3,i2,i1 /)	
		do
			j=3
			do
				if(perm(j)<perm(j+1)) exit
				j=j-1
			enddo
			if(j==0) exit
			l=4
			do
				if(perm(j)<perm(l)) exit
				l=l-1
			enddo
			call swap_int(perm(j),perm(l))
			k=j+1 ; l=4
			do
				if(k>=l) exit
				call swap_int(perm(k),perm(l))
				k=k+1
				l=l-1
			enddo	
			k4(perm(4),perm(3),perm(2),perm(1))=k4(i1,i2,i3,i4)	
		enddo

	end subroutine fill_permutations

	subroutine swap_int(a,b)
	!! swap two integers
		implicit none
		integer, intent(inout) :: a,b
		integer :: tmp

		tmp=a
		a=b
		b=tmp
	end subroutine swap_int

	function linear_interpolation_0D(u,F,u_range,prop_ex,beta_in) result(F_u)
	!! linear interpolation in a specific point of a 1D array
        IMPLICIT NONE
        REAL(dp), intent(in) :: u
			!! @param point where to compute the function
		REAL(dp), intent(in) :: F(:)
			!! @param array containing the function values
		REAL(dp), intent(in) :: u_range(:)
			!! @param array containing the corresponding abscisses
		REAL, intent(in), optional :: prop_ex
			!! @param proportion of the array on which perform the linear extrapolation
			!! (default: 0.05)
		REAL(dp), intent(in), optional :: beta_in(2)
			!! @param optional parameters for the linear extrapolation
        REAL(dp) :: F_u
				REAL(dp) ::  u_min,u_max,du
        INTEGER :: N,i,n_ex
        REAL(dp) :: beta(2)

		N=size(F)
		if(size(u_range)/=N) STOP "Error: sizes of function and range do not match!"

		if(present(prop_ex)) then
			n_ex=nint(prop_ex*N)
		else
        	n_ex=nint(0.05*N)
		endif
		n_ex=max(2,n_ex)
		
    u_max=u_range(N)
		u_min=u_range(1)
		du=(u_max-u_min)/(N-1)
		
		F_u=0._dp
		if(u<u_max .and. u>u_min) then
            i=int(u/du)
            F_u = F(i) + (u-i*du)*(F(i+1)-F(i))/du
    elseif(u>=u_max) then
			!write(0,*) "extrapolation !"
      if(present(beta_in)) then
				beta=beta_in
			else
				beta(2)=(F(N)-F(N-n_ex+1))/(u_range(N)-u_range(N-n_ex+1))
				beta(1)=F(N)-beta(2)*u_max
			endif
			!write(0,*) "beta=",beta
			F_u=beta(1)+beta(2)*u
		elseif(u<=u_min) then
			if(present(beta_in)) then
				beta=beta_in
			else
				beta(2)=(F(n_ex)-F(1))/(u_range(n_ex)-u_range(1))
				beta(1)=F(1)-beta(2)*u_min	
			endif
			F_u=beta(1)+beta(2)*u
    endif
    end function linear_interpolation_0D

	 function linear_interpolation_1D(u,F,u_range,prop_ex,beta_in) result(F_u)
	 !! linear interpolation in a multiple points of a 1D array
        IMPLICIT NONE
        REAL(dp), intent(in) :: u(:)
			!! @param points where to compute the function
		REAL(dp), intent(in) :: F(:)
			!! @param array containing the function values
		REAL(dp), intent(in) :: u_range(:)
			!! @param array containing the corresponding abscisses
		REAL, intent(in), optional :: prop_ex
			!! @param proportion of the array on which perform the linear extrapolation
			!! (default: 0.05)
		REAL(dp), intent(in), optional :: beta_in(2)
			!! @param optional parameters for the linear extrapolation
        REAL(dp) :: F_u(size(u))
        INTEGER :: i
        
        DO i=1,size(u)
        	F_u(i)=linear_interpolation_0D(u(i),F,u_range,prop_ex,beta_in)
        ENDDO        
        
    end function linear_interpolation_1D

	subroutine linear_fit(x,y,lambda,beta)
	!! fit y(x) to a linear function with regularization (L2)
		IMPLICIT NONE
		REAL(dp), intent(in) :: x(:)
			!! @param data locations
		REAL(dp), intent(in) :: y(:)
			!! @param data values
		REAL(dp), intent(in) :: lambda
			!! @param coefficient for the regularization
			!! $$ beta*=argmin(\sum |x*\beta-y|^2 + \lambda*|\beta|^2)   
		REAL(dp), intent(inout) :: beta(:)
			!! @param output coefficients of the linear regression
		REAL(dp), allocatable :: x_ext(:,:), R(:,:), Id(:,:)
		INTEGER :: N, i, INFO

		N=size(x)
		allocate(x_ext(N,1),R(N,N),Id(N,N))
		!write(0,*) N
		Id=0
		do i=1,N ; Id(i,i)=lambda ; enddo

		x_ext(:,1)=x
		!x_ext(:,2)=x(:)
		R=matmul(transpose(x_ext),x_ext)
		beta(2:2)=matmul( &
			sym_mat_inverse(R+Id,INFO) &
			,matmul(transpose(x_ext),y(:)) &
		)
		if(INFO /= 0) &
			STOP "Error: singular matrix for linear regression!"
		beta(1)=y(N)-beta(2)*x(N)
	end subroutine linear_fit

END MODULE matrix_operations


