MODULE timer_module
  use string_operations, only: int_to_str
  IMPLICIT NONE

  TYPE timer_type
    integer :: start_time(8)=-1
    integer :: end_time(8)=-1
    integer :: last_elapsed_time=-1
  CONTAINS
    PROCEDURE :: start => timer_start
    PROCEDURE :: elapsed_time => timer_elapsed_time
  END TYPE timer_type
  

  PRIVATE
  PUBLIC :: timer_type,sec2human

CONTAINS

  SUBROUTINE timer_start(self)
    IMPLICIT NONE
    CLASS(timer_type) :: self

    call date_and_time(values=self%start_time)

  END SUBROUTINE timer_start

  FUNCTION timer_elapsed_time(self)
    IMPLICIT NONE
    CLASS(timer_type) :: self
    REAL :: timer_elapsed_time

    call date_and_time(values=self%end_time)

    timer_elapsed_time = (self%end_time(8) - self%start_time(8))/1000.
    timer_elapsed_time = timer_elapsed_time + (self%end_time(7) - self%start_time(7))
    timer_elapsed_time = timer_elapsed_time + (self%end_time(6) - self%start_time(6))*60.
    timer_elapsed_time = timer_elapsed_time + (self%end_time(5) - self%start_time(5))*3600.

    self%last_elapsed_time = timer_elapsed_time

  END FUNCTION timer_elapsed_time

  function sec2human(sec) result(time)
    real, intent(in) :: sec
    character(:), allocatable :: time
    integer :: secint, dur,D,H,M,S
    
    if(sec < 1.) then
    time = int_to_str(nint(1000.*sec))//" ms"
    return
    endif  
    secint = nint(sec)

    time = ""
    !get number of days
    dur = 3600*24
    D = secint/dur
    secint = mod(secint,dur)
    if(D>0) then
    time = time//int_to_str(D)//"d "
    endif

    !get number of hours
    dur = 3600
    H = secint/dur
    secint = mod(secint,dur)
    if(H>0) then
    time = time//int_to_str(H)//"h "
    endif

    !get number of minutes
    dur = 60
    M = secint/dur
    if(M>0) then
    time = time//int_to_str(M)//"min "
    endif

    !get remaining seconds
    S = mod(secint,dur)
    time = time//int_to_str(S)//"s"

  end function sec2human

END MODULE timer_module