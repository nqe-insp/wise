module system_commons
  use kinds
  use nested_dictionaries
  use periodic_table
  use atomic_units
	use string_operations
	use random
  implicit none

  integer, save :: nat, ndim, ndof
  real(dp), save :: kT, beta, total_mass
	logical :: charges_provided=.FALSE.
  real(dp), allocatable, save :: mass(:), Xinit(:,:), charges(:)
	CHARACTER(2), ALLOCATABLE :: element_symbols(:),element_set(:)

CONTAINS

  subroutine initialize_system(param_library)
	!! CHECK IF XYZ FILE IS GIVEN AS INPUT AND EXTRACT DATA
		IMPLICIT NONE
		TYPE(DICT_STRUCT), intent(in) :: param_library
		CHARACTER(:), allocatable :: xyz_file,xyz_unit
		REAL(dp) :: unit
		LOGICAL :: num_is_indicated,tinker_xyz, blank_line
		INTEGER :: i,j,n_elements,length,spos,n_atoms_of
		INTEGER, ALLOCATABLE :: indices(:),element_indices(:)
		TYPE(DICT_STRUCT), POINTER :: parameters_cat
		CHARACTER(:), ALLOCATABLE :: elements
		REAL(dp), allocatable :: X0_default(:)


		kT = param_library%get("parameters/temperature")
		!! @input_file PARAMETERS/temperature
    beta = 1._dp / kT
		

		call init_periodic_table()
		parameters_cat => param_library%get_child("PARAMETERS")

		xyz_file=parameters_cat%get("xyz_file", default="null")
		!! @input_file PARAMETERS/xyz_file, default ="null"
		write(*,*) "XYZ input : "//xyz_file
		if(xyz_file/="null") then !! XYZ file provided

			xyz_unit=parameters_cat%get("xyz_unit", default="angstrom")
			!! @input_file PARAMETERS/xyz_unit, default="angstrom"
			unit=atomic_units_get_multiplier(xyz_unit)	


			blank_line=.TRUE.
			num_is_indicated=parameters_cat%get("xyz_num_indicated", default=.FALSE.)	
			!! @input_file PARAMETERS/xyz_num_indicated, default=.FALSE.
			tinker_xyz=parameters_cat%get("tinker_xyz", default=.FALSE.)	
			if(tinker_xyz) then
				num_is_indicated=.TRUE. ; blank_line=.FALSE.
			endif

      ndim = 3
			call get_info_from_xyz_file(xyz_file,nat,Xinit,mass,num_is_indicated,blank_line,indices=indices)
			Xinit = Xinit / unit
			mass = mass*au%Mprot
			call parameters_cat%store("element_index",indices)	

		else !! XYZ file NOT provided

      nat = param_library%get("parameters/n_atoms")
			!! @input_file PARAMETERS/n_atoms
      ndim = param_library%get("parameters/n_dim",default=3)
			!! @input_file PARAMETERS/n_dim, default=3
      ndof = nat*ndim

      allocate(Xinit(nat,ndim),X0_default(ndof))
      X0_default(:) = 0._dp
			X0_default=parameters_cat%get("xinit",default=X0_default)
			!! @input_file PARAMETERS/xinit (classical_MD,default=0.)		
			if(size(X0_default) /= ndim*nat) &
				STOP "Error: specified 'xinit' size does not match 'n_dim*n_atoms'"	
			Xinit=RESHAPE(X0_default,(/nat,ndim/))
			deallocate(X0_default)

      !GET mass
      allocate(mass(nat))
      if(param_library%has_key("parameters/mass")) then
        mass=param_library%get("parameters/mass")
        !! @input_file PARAMETERS/mass ,default=1 amu
        if(size(mass) /= nat) &
          STOP "Error: specified 'mass' size does not match 'n_atoms'"
        do i=1,nat
          if(mass(i)<=0) STOP "Error: 'mass' is not properly defined!"
        enddo		
      else
        write(0,*) "Warning: 'mass' not specified, using default of 1 a.m.u. for all atoms."
        mass=au%Mprot
        call param_library%store("parameters/mass",mass)
      endif

    endif

    total_mass = SUM(mass)
    ndof = nat*ndim
    

		call param_library%add_child("ELEMENTS")
		if(allocated(indices)) deallocate(indices)
		allocate(element_symbols(nat), indices(nat))
		element_symbols="Xx"
		if(parameters_cat%has_key("element_index")) then
			indices=parameters_cat%get("element_index")
			!! @input_file PARAMETERS/element_index
			if(size(indices) /= nat) &
				STOP "Error: specified 'element_index' size does not match 'n_atoms'"
			elements=""
			n_elements=0
			DO i=1,nat
				element_symbols(i)=get_atomic_symbol(indices(i))
				if(index(elements,trim(element_symbols(i)))==0) then
					n_elements=n_elements+1
					elements=elements//"_"//trim(element_symbols(i))
				endif
			ENDDO
			allocate(element_set(n_elements),element_indices(nat))	
			DO i=1,n_elements
				length=len(elements)
				spos=index(elements,"_",BACK=.TRUE.)
				element_set(i)=elements(spos+1:length)
				call param_library%add_child("ELEMENTS/"//element_set(i))
				if(spos>1) elements=elements(1:spos-1)
				n_atoms_of=0
				DO j=1,nat
					if(element_set(i) == element_symbols(j)) then
						n_atoms_of=n_atoms_of+1
						element_indices(n_atoms_of) = j 
					endif
				ENDDO
				call param_library%store("ELEMENTS/"//element_set(i)//"/n",n_atoms_of)
				call param_library%store("ELEMENTS/"//element_set(i)//"/symbol",element_set(i))
				call param_library%store("ELEMENTS/"//element_set(i)//"/indices",element_indices(1:n_atoms_of))
			ENDDO
			write(*,*) "n_elements= "//int_to_str(n_elements)//" -> ",element_set//" "
		else
			n_elements=1
			allocate(element_set(n_elements))
			allocate(element_indices(nat))	
			element_set(1)="Xx"
			DO j=1,nat ; element_indices(j)=j ; ENDDO
			call param_library%add_child("ELEMENTS/"//element_set(1))
			call param_library%store("ELEMENTS/"//element_set(1)//"/n",nat)
			call param_library%store("ELEMENTS/"//element_set(1)//"/symbol",element_set(1))
			call param_library%store("ELEMENTS/"//element_set(1)//"/indices",element_indices(1:nat))
		endif

		call initialize_charges(param_library)
	
	end subroutine initialize_system

	subroutine initialize_charges(param_library)
		implicit none
		TYPE(DICT_STRUCT), intent(in) :: param_library
		TYPE(DICT_STRUCT), POINTER :: charge_lib
		CHARACTER(DICT_KEY_LENGTH), ALLOCATABLE :: keys(:)
		LOGICAL, ALLOCATABLE :: is_sub(:)
		REAL(dp) :: charge_tmp
		INTEGER :: i
		CHARACTER(2) :: symbol
		
		if(.not. param_library%has_key("PARAMETERS/charges")) then
			charges_provided = .FALSE.
			return
		endif

		allocate(charges(nat))
		charges_provided=.TRUE.
		charge_lib => param_library%get_child("PARAMETERS/charges")
		write(*,*)
		write(*,*) "Charges:"
		call dict_list_of_keys(charge_lib,keys,is_sub)
		DO i=1,size(keys)
			if(is_sub(i)) CYCLE
			charge_tmp=charge_lib%get(keys(i))
			symbol=trim(keys(i))
			symbol(1:1)=to_upper_case(symbol(1:1))
			write(*,*) symbol," : ", charge_tmp,"au"
		ENDDO
		write(*,*)
		do i=1, nat
			charges(i)=charge_lib%get(trim(element_symbols(i)),default=-1001._dp)
			if(charges(i)<=-1000._dp) then
				write(0,*) "Error: incorrect charge for element "//trim(element_symbols(i))
				STOP "Execution stopped."
			endif             
		enddo

	end subroutine initialize_charges

	subroutine sample_classical_momentum(P)
		implicit none
		real(dp), intent(inout) :: P(:,:)
		integer :: i,j

		do j=1,ndim
			call randGaussN(P(:,j))
			P(:,j)=P(:,j)*sqrt(mass(:)*kT)
		enddo

	end subroutine sample_classical_momentum

	subroutine get_dipole(P,mu)
		implicit none
		real(dp), intent(in) :: P(:,:)
		real(dp), intent(inout) :: mu(:)
		integer :: i,j

		mu(:)=0._dp
		DO j=1,ndim ; DO i=1,nat
			mu(j)=mu(j) + charges(i)*P(i,j)/(mass(i)*nat)					
		ENDDO ; ENDDO

	end subroutine get_dipole

end module system_commons