module wigner_sampler
  use kinds
  use wigner_forces
  use system_commons
  use nested_dictionaries
  use file_handler
  use wigner_forces, only : WIG_AUX_SIM_TYPE
  use matrix_operations
  implicit none

  real(dp), save :: gamma
  real(dp), save :: dt,dt2
  real(dp), save :: gamma_exp
  real(dp), allocatable, save :: sigma(:)
  logical,save :: nose_correction
  real(dp), save :: nose=1, Mnose=1
  integer, save :: avg_counter
  real(dp), save :: temp_avg, nose_avg

  integer, save :: ux,up,uf,ufvar,unose
  logical, save :: save_traj
  logical, save :: fast_forward_langevin
  CHARACTER(*), PARAMETER :: xfilename="position.sampler.traj"
  CHARACTER(*), PARAMETER :: pfilename="momentum.sampler.traj"
  CHARACTER(*), PARAMETER :: ffilename="force.sampler.traj"
  CHARACTER(*), PARAMETER :: fvarfilename="force.stddev.sampler.traj"
  CHARACTER(*), PARAMETER :: nosefilename="nose.sampler.traj"

  PRIVATE
  PUBLIC :: initialize_sampler, sampler_propagation &
            ,sample_wigner_momentum_standard &
            ,sample_wigner_momentum_penalty &
            ,compute_kinetic_energy

contains

  subroutine initialize_sampler(param_library)
    implicit none
    TYPE(DICT_STRUCT), intent(in) :: param_library
    integer :: stat

    dt = param_library%get("sampler/dt")
    dt2 = 0.5_dp*dt

    gamma = param_library%get("sampler/gamma")
    gamma_exp = exp(-gamma*dt)
    allocate(sigma(nat))
    sigma(:) = SQRT( mass(:)*kT*( 1._dp - exp(-2*gamma*dt) ) )

    save_traj = param_library%get("sampler/save_sampler_trajectory",default=.FALSE.)

    fast_forward_langevin = param_library%get("sampler/sampler_fast_forward_langevin",default=.TRUE.)
    if(fast_forward_langevin) write(*,*) "fast forward langevin for sampler"

    nose_correction = param_library%get("sampler/sampler_nose_correction",default=.FALSE.)
    nose=0._dp
    if(nose_correction) then
      write(*,*) "nose correction for sampler"
      Mnose = param_library%get("sampler/M_nose",default=1.0e6_dp)
    endif

    temp_avg = 0._dp
    nose_avg = 0._dp
    avg_counter = 0

    open(newunit=ux, iostat=stat, file=output%working_directory//xfilename, status='old')
    if (stat == 0) close(ux, status='delete')
    open(newunit=ux, iostat=stat, file=output%working_directory//pfilename, status='old')
    if (stat == 0) close(ux, status='delete')
    open(newunit=ux, iostat=stat, file=output%working_directory//ffilename, status='old')
    if (stat == 0) close(ux, status='delete')
    open(newunit=ux, iostat=stat, file=output%working_directory//fvarfilename, status='old')
    if (stat == 0) close(ux, status='delete')
    open(newunit=ux, iostat=stat, file=output%working_directory//nosefilename, status='old')
    if (stat == 0) close(ux, status='delete')
    

  end subroutine initialize_sampler

  subroutine sampler_propagation(X,P,nsteps,auxsim,verbose)
    implicit none
    real(dp), intent(inout) :: X(:,:), P(:,:)
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    integer, intent(in) :: nsteps
    logical, intent(in) :: verbose
    integer :: istep
    real(dp) :: temp,Ekin,temp_sum, nose_sum

    if(save_traj) then
      open(newunit=ux,file=output%working_directory//xfilename,position="append")
      open(newunit=up,file=output%working_directory//pfilename,position="append")
      open(newunit=uf,file=output%working_directory//ffilename,position="append")
      if(auxsim%num_aux_sim > 1) then
        open(newunit=ufvar,file=output%working_directory//fvarfilename,position="append")
      endif
      if(nose_correction) then
        open(newunit=unose,file=output%working_directory//nosefilename,position="append")
      endif
    endif

    nose_sum = 0.
    temp_sum = 0.

    avg_counter = avg_counter + 1

    do istep = 1,nsteps      

      P(:,:) = P(:,:) + dt2*auxsim%Fw(:,:)
      call apply_A(X,P,dt2)

      if(nose_correction) then
        call compute_kinetic_energy(P,Ekin,temp)
        call apply_N(Ekin,dt2)
      endif

      if(fast_forward_langevin) then
        call apply_O_FFL(P)
      else
        call apply_O(P)
      endif
      call compute_kinetic_energy(P,Ekin,temp)      
      temp_sum = temp_sum + temp

      if(nose_correction) then
        call apply_N(Ekin,dt2)
        nose_sum = nose_sum + nose
      endif

      call apply_A(X,P,dt2)
      call compute_wigner_forces(X,auxsim)
      P(:,:) = P(:,:) + dt2*auxsim%Fw(:,:)

      if(save_traj) then
        write(ux,*) X(:,:)
        write(up,*) P(:,:)
        write(uf,*) auxsim%Fw(:,:)
        if(auxsim%num_aux_sim > 1) then
          write(ufvar,*) sqrt(auxsim%Fvar(:,:))
        endif
        if(nose_correction) write(unose,*) nose,gamma 
      endif

    enddo

    nose_avg = nose_avg + (nose_sum - nose_avg)/avg_counter
    temp_avg = temp_avg + (temp_sum/nsteps-temp_avg)/avg_counter    

    if(verbose) then
      write(*,*) "langevin_temperature= ",real(temp_avg,sp),"K"
      if(nose_correction) then
        write(*,*) "average_nose= ",real(nose_avg*au%THz,sp),"THz (gamma=",real(gamma*au%THz,sp),"THz)"
      endif
      temp_avg=0._dp
      nose_avg=0._dp 
      avg_counter=0
    endif

    if(save_traj) then
      close(ux) ; close(up) ; close(uf)
      if(auxsim%num_aux_sim > 1) close(ufvar)
      if(nose_correction) close(unose)
    endif

  end subroutine sampler_propagation

  subroutine apply_A(X,P,tau)
    implicit none
    real(dp), intent(inout) :: X(:,:)
    real(dp), intent(in) :: P(:,:)
    real(dp), intent(in) :: tau
    integer :: j

    do j=1,ndim
      X(:,j) = X(:,j) + tau*P(:,j)/mass(:)
    enddo

  end subroutine apply_A

  subroutine apply_O(P)
    implicit none
    real(dp), intent(inout) :: P(:,:)
    integer :: j
    real(dp) :: R(nat), gexp

    gexp=gamma_exp*exp(-nose*dt)

    do j=1,ndim
      call RandGaussN(R)
      P(:,j) = P(:,j)*gexp + sigma(:)*R(:)
    enddo

  end subroutine apply_O

  subroutine apply_O_FFL(P)
    implicit none
    real(dp), intent(inout) :: P(:,:)
    integer :: i
    real(dp) :: R(ndim), gexp
    real(dp) :: Pold, Pnew

    gexp=gamma_exp*exp(-nose*dt)

    do i=1,nat
      call RandGaussN(R)
      Pold = NORM2(P(i,:))
      Pnew = NORM2(P(i,:)*gexp + sigma(i)*R(:))
      P(i,:) = Pnew * P(i,:) / Pold
    enddo

  end subroutine apply_O_FFL

  subroutine apply_N(Ekin,tau)
		IMPLICIT NONE
		real(dp), intent(in) :: Ekin
		REAL(dp), INTENT(in) :: tau

		nose=nose + (tau/Mnose) &
			*(Ekin - 0.5_dp*ndof*kT)

	end subroutine apply_N

  subroutine compute_kinetic_energy(P,Ekin,temp)
    IMPLICIT NONE
		real(dp), intent(in) :: P(:,:)
    real(dp), intent(out) :: Ekin,temp
    integer :: j

    temp=0._dp
    Ekin = 0._dp
    do j=1,ndim
      Ekin = Ekin + SUM(P(:,j)**2/(2*mass(:)))
    enddo
    temp = au%kelvin*2._dp*Ekin/ndof

  end subroutine

!---------------------------------------------------------

subroutine sample_wigner_momentum_standard(P,KinWeight,auxsim)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    real(dp), intent(inout) :: P(:,:,:)
    real(dp), intent(out) :: KinWeight
    integer :: c, i,j,k,nsamples, isample, INFO
    real(dp), allocatable :: P0(:),thetaInv(:,:),sqrtTheta(:),EMat(:,:)

    nsamples = size(P,dim=3)    

    allocate(thetaInv(ndof,ndof),sqrtTheta(ndof),EMat(ndof,ndof))
    !mass normalization
    c=0
		DO j=1,ndim; do i=1,nat
				c=c+1
				DO k=1,ndim
						thetaInv((k-1)*nat+1:k*nat,c) &
								=auxsim%k2((k-1)*nat+1:k*nat,c) &
										*sqrt(mass(i)*mass(:))
				ENDDO
		ENDDO ; ENDDO

    call sym_mat_diag(thetaInv,sqrtTheta,EMat,INFO)     
    IF(INFO/=0) STOP "Error: could not diagonalize thetaInv"  
    sqrtTheta = sqrt(1._dp/sqrtTheta)
    KinWeight = product(sqrt(beta)*sqrtTheta)

    deallocate(thetaInv)   
    allocate(P0(ndof))
    DO isample = 1,nsamples
      call RandGaussN(P0)
      P0 = P0*sqrtTheta
      P(:,:,isample) = RESHAPE( matmul(EMat,P0), (/nat,ndim/) )
      DO i=1,ndim
        P(:,i,isample)=P(:,i,isample)*sqrt(mass(:))
      ENDDO
    ENDDO    

  end subroutine sample_wigner_momentum_standard

  subroutine sample_wigner_momentum_penalty(P,KinWeight,auxsim, &
                    nsteps_penalty_therm,nsteps_penalty_btwn_samples)
    implicit none
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    real(dp), intent(inout) :: P(:,:,:)
    real(dp), intent(out) :: KinWeight
    integer, intent(in) :: nsteps_penalty_therm, nsteps_penalty_btwn_samples
    integer :: i,nsamples, isample, INFO,naccepted,nsteps_tot
    integer :: n_eta_too_big
    real(dp) :: eta
    real(dp), allocatable :: P0(:),sqrtk2inv(:),EMat(:,:),Pnew(:)

    nsamples = size(P,dim=3)    

    allocate(sqrtk2inv(ndof),EMat(ndof,ndof))

    call sym_mat_diag(auxsim%k2,sqrtk2inv,EMat,INFO)   
    IF(INFO/=0) STOP "Error: could not diagonalize k2"  
    sqrtk2inv = sqrt(1._dp/sqrtk2inv)
    KinWeight = product(sqrtk2inv)*(product(sqrt(beta/mass(:))))**ndim

    allocate(P0(ndof),Pnew(ndof))
    call RandGaussN(P0)
    P0 = matmul(EMat,P0*sqrtk2inv)
    naccepted=0
    n_eta_too_big=0
    auxsim%eta_penalty=0._dp
    DO i = 1, nsteps_penalty_therm
      call RandGaussN(Pnew)
      Pnew = matmul(EMat,Pnew*sqrtk2inv)
      call penalty_test(P0,Pnew,auxsim,naccepted,eta,n_eta_too_big)
      auxsim%eta_penalty = auxsim%eta_penalty + eta
    ENDDO
    P(:,:,1) = RESHAPE( P0, (/nat,ndim/) )

    if(nsamples>1) then
      DO isample = 2,nsamples        
        do i=1, nsteps_penalty_btwn_samples
          call RandGaussN(Pnew)
          Pnew = matmul(EMat,Pnew*sqrtk2inv)
          call penalty_test(P0,Pnew,auxsim,naccepted,eta,n_eta_too_big)
          auxsim%eta_penalty = auxsim%eta_penalty + eta
        enddo
        P(:,:,isample) = RESHAPE( P0, (/nat,ndim/) )
      ENDDO    
    endif

    nsteps_tot = nsteps_penalty_therm + (nsamples-1)*nsteps_penalty_btwn_samples
    auxsim%accratio_penalty = REAL(naccepted,dp) / REAL(nsteps_tot,dp)
    auxsim%eta_too_big_ratio = REAL(n_eta_too_big,dp) / REAL(nsteps_tot,dp)
    auxsim%eta_penalty = auxsim%eta_penalty / REAL(nsteps_tot,dp)

    !write(*,*) "n_penalty_test=",nsteps_tot,"; n_accepted=",naccepted,"; n_eta_too_big=",n_eta_too_big

  end subroutine sample_wigner_momentum_penalty

  subroutine penalty_test(P0,Pnew,auxsim,naccepted,eta,n_eta_too_big)
    implicit none
    real(dp), intent(inout) :: P0(:)
    real(dp), intent(in) :: Pnew(:)
    type(WIG_AUX_SIM_TYPE), intent(inout) :: auxsim
    real(dp), intent(out) :: eta
    integer, intent(inout) :: naccepted,n_eta_too_big
    real(dp) :: xi2,normdiff2,Psum(ndof),R
    real(dp) :: uB
    integer :: i

    normdiff2=0
    do i=1,ndof
      normdiff2 = normdiff2 + (P0(i)-Pnew(i))**2
    enddo
    Psum = P0+Pnew

    xi2 = 0.25_dp*normdiff2*dot_product(Psum,matmul(auxsim%k2xi2,Psum))
    eta = xi2/auxsim%num_aux_sim
    if(eta>=0.25_dp) then
      n_eta_too_big = n_eta_too_big + 1
      !write(0,*) "Warning: penalty eta=",eta,">=0.25, the variance of k2 is too high!"
    endif
    uB= 0.5_dp*xi2 + 0.25_dp*xi2*xi2/(auxsim%num_aux_sim+1)
    call random_number(R)
    if(R <= exp(-uB) ) then
      P0=Pnew
      naccepted = naccepted+1
    endif

  end subroutine penalty_test

end module wigner_sampler