MODULE input_handler
	USE kinds
	USE string_operations
	USE nested_dictionaries
	USE atomic_units
	USE file_handler
	USE trajectory_files
	IMPLICIT NONE

	! TYPE FILE_STRUCT
	! 	CHARACTER(:), allocatable :: key

	! 	CHARACTER(:), allocatable :: name
	! 	INTEGER :: stride
	! 	LOGICAL :: formatted
	! 	CHARACTER(:), allocatable :: unit

	! 	LOGICAL :: in_file_def
	! 	LOGICAL :: has_time_column
	! END TYPE FILE_STRUCT

	TYPE(TRAJ_FILE) :: file_tmp
	LOGICAL :: in_file_def
	CHARACTER(:), allocatable :: file_tmp_key

CONTAINS

	subroutine parse_input_file(filename,dict)
		IMPLICIT NONE
		CHARACTER(*), INTENT(in) :: filename
		class(DICT_STRUCT), intent(inout) :: dict

		CHARACTER (LEN = 256) :: line
    	CHARACTER (LEN = 256), DIMENSION(10000) :: fields
    	CHARACTER (LEN = 32), DIMENSION(10000)  :: field_types
		INTEGER :: iostatus, nfields,u,i,cat_size
		CHARACTER(:), allocatable :: current_category, keyword, path,cat_fields
		LOGICAL :: file_exists,new_cat

		in_file_def=.FALSE.
		current_category=""
		path=""

		INQUIRE( FILE=trim(filename), EXIST=file_exists)
		IF( .not. file_exists ) THEN
			write(0,*) "Error: input file '"//trim(filename)//"' does not exist!"
			STOP "Execution stopped."
		ENDIF

		OPEN( FILE=trim(filename), NEWUNIT=u , IOSTAT=iostatus, STATUS='OLD')
		IF( iostatus /= 0 ) THEN
			write(0,*) "Error: could not open input file: "//trim(filename)
			STOP "Execution stopped."
		ENDIF
		
		DO
			READ( UNIT=u, FMT='(A256)', IOSTAT=iostatus ) line
        	IF( iostatus /= 0 ) EXIT

			CALL split_line( line, nfields, fields, field_types )

			keyword=to_lower_case(TRIM(fields(1)))
			if(keyword=="") CYCLE !skip blank lines

			! CHECK IF STARTING A NEW CATEGORY
			cat_fields=""
			DO i=1,nfields
				cat_fields=cat_fields//TRIM(fields(i))
			ENDDO
			cat_size=len(cat_fields)
			new_cat=.FALSE.
			IF( cat_fields(cat_size:cat_size)=="{" )  THEN
				path=trim(path)//PATH_SEP//trim(cat_fields(:cat_size-1))
				new_cat=.TRUE.
			ELSEIF(cat_fields(1:1)=="&" ) THEN !RETROCOMPATIBILITY
				path=trim(path)//PATH_SEP//trim(cat_fields(2:))
				new_cat=.TRUE.					
			ENDIF
			IF( new_cat) THEN				
				call dict%add_child(path)
				!write(*,*) "Added category '"//path//"'."
				CYCLE								
			ENDIF

			!OPEN AND CLOSE CATEGORY ON SAME LINE
			IF(cat_size>1) THEN
				IF( cat_fields(cat_size-1:cat_size)=="{}" )  THEN
					call dict%add_child(path//PATH_SEP//trim(cat_fields(:cat_size-2)))
					!write(*,*) "Added category '"//trim(path)//PATH_SEP//trim(cat_fields(:cat_size-2))//"'."
					CYCLE
				ENDIF
			ENDIF

			! NO KEYWORD OUT OF A CATEGORY
			IF( path == "") THEN
				if(keyword(1:1)=="}" .OR. keyword(1:1)=="/" .OR. index(keyword,"&end")/= 0) &
					WRITE(0,*) "Warning: closed a category while none was open"
				STOP "Error: line not recognized or instruction out of a category!"
			endif

			!CHECK IF END OF A CATEGORY
			if(keyword(1:1)=="}" .OR. keyword(1:1)=="/" .OR. index(keyword,"&end")/= 0 ) then
				path=get_parent_path(path)
				CYCLE
			endif

			!write(*,*) line
			!IF SURVIVED TO HERE, ADD LINE TO dict
			! if(current_category=="OUTPUT") then
			! 	call add_line_to_output_cat(dict,nfields,fields,field_types)
			! else
			call add_line_to_dict(dict,nfields,fields,field_types,path)			
			! endif
		ENDDO
		
		CLOSE(u)
		
	end subroutine parse_input_file

	subroutine add_line_to_dict(dict, nfields, fields, field_types,path)
		IMPLICIT NONE
		class(DICT_STRUCT), intent(inout) :: dict
		INTEGER, INTENT(in) :: nfields
		CHARACTER(*), INTENT(in) :: path
		CHARACTER(*), DIMENSION(:), INTENT(in) :: fields
		CHARACTER(*), DIMENSION(:), INTENT(in) :: field_types

		REAL(dp) :: real_field
		INTEGER :: int_field
		LOGICAL :: logical_field
		CHARACTER(:), allocatable :: string_line,key,array_type,unit
		REAL(dp), allocatable :: real_array_line(:)
		INTEGER, allocatable :: int_array_line(:)
		LOGICAL, allocatable :: logical_array_line(:)
		INTEGER :: i

		call get_unit(to_lower_case(trim(fields(1))),key,unit)
		key=path//PATH_SEP//key
		!write(*,*) key
		if(dict%has_key(key)) then
			write(0,*) "Error: key '"//key//"' is present multiple times &
						&in category '"//path//"' !"
			STOP "Execution stopped."
		endif

		if(nfields==2) then

			SELECT CASE(field_types(2))
				CASE(logical_type)
					logical_field=string_to_logical(fields(2))
					call dict%store(key,logical_field)
				CASE(int_type)
					if(unit /= "") then
						READ(fields(2),*) real_field
						real_field=convert_to_atomic_units(real_field,unit)
						call dict%store(key,real_field)
					else
						READ(fields(2),*) int_field
						call dict%store(key,int_field)
					endif				
				
				CASE(real_type)
					READ(fields(2),*) real_field
					real_field=convert_to_atomic_units(real_field,unit)
					call dict%store(key,real_field)

				CASE(string_type)
					call dict%store(key,trim(fields(2)))

				CASE DEFAULT
					STOP "Error: Unknown type!"
			END SELECT

		else if(nfields > 2) then

			! if(field_types(2)==logical_type) THEN
			! 	array_type=string_type
			! ELSE
			! 	array_type=field_types(2)
			! ENDIF
			array_type=field_types(2)
			DO i=3,nfields
				IF(field_types(i)/=array_type) THEN
					if(field_types(i)==real_type .AND. array_type==int_type) then
						array_type=real_type
					elseif(field_types(i)==int_type .AND. array_type==real_type) then
						array_type=real_type
					else
						STOP "Error: cannot handle inhomogeneous arrays!"
					endif					
				ENDIF
			ENDDO

			SELECT CASE(array_type)
			CASE(int_type)
				if(unit /= "") then
					allocate(real_array_line(nfields-1))
					READ(fields(2:nfields),*) real_array_line
					do i=1,nfields-1
						real_array_line(i)=convert_to_atomic_units(real_array_line(i),unit)
					enddo
					call dict%store(key,real_array_line)
				else
					allocate(int_array_line(nfields-1))
					READ(fields(2:nfields),*) int_array_line
					call dict%store(key,int_array_line)
				endif
				

			CASE(real_type)
				allocate(real_array_line(nfields-1))
				READ(fields(2:nfields),*) real_array_line
				do i=1,nfields-1
					real_array_line(i)=convert_to_atomic_units(real_array_line(i),unit)
				enddo
				call dict%store(key,real_array_line)
			
			CASE(logical_type)
				allocate(logical_array_line(nfields-1))
				DO i=2,nfields
					logical_array_line(i-1)=string_to_logical(fields(i))
				ENDDO
				call dict%store(key,logical_array_line)

			CASE(string_type)
				string_line=""
				do i=2,nfields
					string_line=string_line//" "//trim(fields(i))
				enddo
				call dict%store(key,string_line)

			CASE DEFAULT
				STOP "Error: cannot handle inhomogeneous arrays!"
			END SELECT
		endif

	end subroutine add_line_to_dict

	subroutine get_unit(word,key,unit)
		IMPLICIT NONE
		CHARACTER(*), INTENT(in) :: word
		CHARACTER(:), allocatable, intent(inout) :: key,unit

		INTEGER :: unit_start, n

		unit_start=max(index(word,"["),index(word,"{"))
		if(unit_start<=0) then
			key=word
			unit=""
		elseif(unit_start==1) then
			write(0,*) "Error: Field '"//word//"' must not start with '[' or '{' !"
			STOP "Execution stopped."
		else
			key=word(:unit_start-1)
			n=len(word)
			if(word(n:n) /= "]" .AND. word(n:n) /= "}") then
				write(0,*) "Error: wrong unit specification in field '"//word//"' !"
				STOP "Execution stopped."
			else
				if( n-unit_start-2 < 0 ) then
					unit=""
				else
					unit=word(unit_start+1:n-1)
				endif
			endif
		endif
	end subroutine get_unit

	! subroutine add_line_to_output_cat(dict, nfields, fields, field_types)
	! 	IMPLICIT NONE
	! 	class(CAT_STRUCT), intent(inout) :: dict
	! 	INTEGER, INTENT(in) :: nfields
	! 	CHARACTER(*), DIMENSION(:), INTENT(in) :: fields
	! 	CHARACTER(*), DIMENSION(:), INTENT(in) :: field_types
	! 	INTEGER :: n, i_file,i
	! 	CHARACTER(:), allocatable :: line_compact,key,array_type,unit

	! 	line_compact=""
	! 	do i=1,nfields
	! 		line_compact=line_compact//trim(fields(i))
	! 	enddo
	! 	line_compact=to_lower_case(line_compact)

	! 	n=len(line_compact)
	! 	if(n==1 .and. line_compact(1:1)=="}") then

	! 		if(.not. in_file_def) &
	! 			STOP "Error: closed a file definition while none was open!"
	! 		call output%add_file(file_tmp,index=i_file)
	! 		call dict%store(file_tmp_key,"OUTPUT",.g.i_file)
	! 		in_file_def=.FALSE.

	! 	elseif(n>2 .and. line_compact(n-1:n)=="{}") then

	! 		if(in_file_def) &
	! 			STOP "Error: opened a file definition while another was not closed!"
	! 		call output%create_file(line_compact(:n-2),index=i_file)
	! 		call dict%store(line_compact(:n-2),"OUTPUT",.g.i_file)

	! 	elseif(n>1 .and. line_compact(n:n)=="{") then

	! 		if(in_file_def) &
	! 			STOP "Error: opened a file definition while another was not closed!"
	! 		file_tmp_key=line_compact(:n-1)
	! 		call file_tmp%init(line_compact(:n-1))
	! 		in_file_def=.TRUE.

	! 	elseif(nfields==2) then

	! 		key=to_lower_case(trim(fields(1)))

	! 		if(.not. in_file_def) then

	! 			SELECT CASE(key)
	! 			CASE("formatted_by_default")
	! 				if(field_types(2) /= logical_type) &
	! 					STOP "Error: type for 'formatted_by_default' should be LOGICAL"
	! 				DEFAULT_FORMATTED=string_to_logical(fields(2))
	! 			CASE("time_column_by_default")
	! 				if(field_types(2) /= logical_type) &
	! 					STOP "Error: type for 'time_column_by_default' should be LOGICAL"
	! 				DEFAULT_TIME_COL=string_to_logical(fields(2))
	! 			CASE DEFAULT
	! 				write(0,*) "Error: unrecognized line: "//line_compact
	! 				STOP "Execution stopped."
	! 			END SELECT

	! 		else
				
	! 			SELECT CASE(key)
	! 			CASE("name")
	! 				if(field_types(2) /= string_type) &
	! 					STOP "Error: type for 'name' should be STRING"
	! 				file_tmp%name=trim(fields(2))
	! 			CASE("time_column")
	! 				if(field_types(2) /= logical_type) &
	! 					STOP "Error: type for 'time_column' should be LOGICAL"
	! 				file_tmp%has_time_column=string_to_logical(fields(2))
	! 			CASE("formatted")
	! 				if(field_types(2) /= logical_type) &
	! 					STOP "Error: type for 'formatted' should be LOGICAL"
	! 				call file_tmp%update_format(formatted=string_to_logical(fields(2)))
	! 			CASE("unit")
	! 				if(field_types(2) /= string_type) &
	! 					STOP "Error: type for 'unit' should be STRING"
	! 				file_tmp%default_unit=trim(fields(2))
	! 			CASE("stride")
	! 				if(field_types(2) /= int_type) &
	! 					STOP "Error: type for 'stride' should be INTEGER"
	! 				READ(fields(2),*) file_tmp%stride
	! 			CASE DEFAULT
	! 				write(0,*) "Error: unrecognized keyword: "//key
	! 				STOP "Execution stopped."
	! 			END SELECT
	! 		endif

	! 	else
	! 		write(0,*) "Error: unrecognized line: "//line_compact
	! 		STOP "Execution stopped."
	! 	endif
	
	! end subroutine add_line_to_output_cat

END MODULE input_handler
