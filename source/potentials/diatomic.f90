module diatomic
  use kinds
  USE potential
  USE atomic_units
  USE nested_dictionaries
  USE string_operations, only: int_to_str,to_lower_case
  IMPLICIT NONE

  ABSTRACT INTERFACE
		subroutine pot_sub(dr,v,dv)
         import dp
			IMPLICIT NONE
			REAL(dp), INTENT(in):: dr
         REAL(dp), INTENT(out) :: v,dv
		end subroutine pot_sub
   END INTERFACE

  PROCEDURE(pot_sub),pointer,SAVE ::  potptr

  CHARACTER(*), PARAMETER :: LIB="POTENTIAL/diatomic/"

  REAL(dp), SAVE :: vo, sig, ro, omega0,ko
  CHARACTER(:), ALLOCATABLE :: interaction_type

  INTEGER, SAVE :: n_atoms

  PRIVATE
  PUBLIC :: get_diatomic_pot_info, Pot_diatomic, dPot_diatomic,hessian_diatomic &
            ,initialize_diatomic

CONTAINS

SUBROUTINE initialize_diatomic(param_library)
    IMPLICIT NONE
    TYPE(DICT_STRUCT), INTENT(IN) :: param_library
    TYPE(DICT_STRUCT), POINTER :: pot_lib
    INTEGER :: i
    LOGICAL :: isDeuterium
    REAL(dp), ALLOCATABLE :: mass_HO(:)
    REAL(dp) :: m_reduce

    pot_lib => param_library%get_child(LIB)

    n_atoms=param_library%get("PARAMETERS/n_atoms")
    
    omega0=pot_lib%get("omega0",default=3500./au%cm1)   
    sig=pot_lib%get("sigma",default=1.21022828_dp)
    ro=pot_lib%get("ro",default=1.779933_dp)

    !GET MASS 
    if(param_library%has_key("parameters/mass")) then
      mass_HO=param_library%get("parameters/mass")
      !! @input_file PARAMETERS/mass (potentials)
      if(size(mass_HO) /= n_atoms) &
        STOP "Error: specified 'mass' size does not match 'n_atoms'"			
    else
      write(0,*) "Warning: 'mass' not specified, using default of 1 a.m.u. for all dimensions."
      allocate(mass_HO(n_atoms))
      mass_HO=au%Mprot
    endif

    m_reduce=mass_HO(1)*mass_HO(2)/(mass_HO(1)+mass_HO(2))
    vo = 0.5*m_reduce*(omega0/sig)**2    

    ko=0.5*m_reduce*omega0**2

    interaction_type = pot_lib%get("type",default="morse")
    interaction_type=to_lower_case(interaction_type)
    SELECT CASE(interaction_type)
    CASE("morse")
      potptr => Morse_diatomic
      write(*,*) 'DIATOMIC MORSE'
    CASE("harmonic")
      potptr => harm_diatomic
      write(*,*) 'DIATOMIC HARMONIC'
    CASE default
      write(0,*) "Error: "//interaction_type//" interaction is not implemented."
      STOP "Execution stopped."
    END SELECT

END SUBROUTINE initialize_diatomic

SUBROUTINE get_diatomic_pot_info(X,Pot,Forces,hessian,vir)
   IMPLICIT NONE
   real(dp), intent(in)  :: X(:,:)
   real(dp), intent(out) :: Forces(size(X,1),size(X,2))
   real(dp), intent(out) :: Pot
   real(dp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
   real(dp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))
   real(dp) :: vir_tmp(size(X,2),size(X,2))
   real(dp) :: box(3)
   INTEGER :: na

   call ffdiatomic(X,size(X,1),Forces,Pot,vir_tmp)
   if(present(vir)) vir = vir_tmp
   if(present(hessian)) hessian = hessian_diatomic(X)

END SUBROUTINE get_diatomic_pot_info

function Pot_diatomic(X) result(U)
   implicit none
   real(dp),intent(in)  :: X(:,:)
   REAL(dp) :: U
   INTEGER :: it
   REAL(dp) :: F(size(X,1),size(X,2))
   real(dp) :: vir_tmp(3,3)

   call ffdiatomic(X,size(X,1),F,U,vir_tmp)

end function Pot_diatomic

function dPot_diatomic(X) result(dU)
   implicit none
   real(dp),intent(in)  :: X(:,:)
   real(dp) :: dU(size(X,1),size(X,2))
   INTEGER :: it
   REAL(dp) :: alpha_x,alpha_y
   real(dp) :: vir_tmp(3,3), U

   call ffdiatomic(X,size(X,1),dU,U,vir_tmp)

   dU=-dU

end function dPot_diatomic

function hessian_diatomic(X) result(H)
   implicit none
   real(dp),intent(in)  :: X(:,:)
   real(dp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))

   STOP "Hessian not implemented for diatomic potential"
   
end function hessian_diatomic

subroutine ffdiatomic(rt,na,dvdrt,v, vir)
  use atomic_units
  implicit none  
  integer :: i,j,na,nm,ic,njump
  real(dp) :: r(3,na), dvdr(3,na), rt(na,3), dvdrt(na,3)
  real(dp) :: oo_eps, oo_sig, rcut,v,vlj,vint
  real(dp) :: ks,kb, angstrom,alp,de
  real(dp) :: qc,qo,theta,reco,vir(3,3), virlj(3,3), virint(3,3)

  ! Set and check number of molecules.
  ! 
  if (mod(na,2)/=0) stop 'ERROR 1 in POTENTIAL'

  ! Zero-out potential and derivatives.
  !
  v = 0.d0
  vir = 0.d0
  dvdr(:,:) = 0.d0

  do i=1, na 
    r(:,i)=rt(i,:)
  enddo


  call interact_diatomic(r,dvdr,v,vir,na)

  ! write(*,*) "force="
  ! write(*,*) dvdr

  ! ....and we're done...
  !
  vir = -1.0d0 *vir
  do i=1, na 
    dvdrt(i,:) = -dvdr(:,i)
  enddo
  return
end Subroutine ffdiatomic

Subroutine interact_diatomic(r,dvdr,v,vir,na)
  implicit none
  integer :: na,i,j
  real(dp) :: r(3,na),dvdr(3,na),v,vir(3,3)
  real(dp) :: dr,fij,dfx,dfy,dfz
  real(dp) :: dx,dy,dz,vscale,dscale
  
  v = 0.d0
  dvdr(:,:) = 0.d0
  vir = 0.d0

  do i = 1,na-1,2    
    dx = r(1,i)-r(1,i+1)
    dy = r(2,i)-r(2,i+1)
    dz = r(3,i)-r(3,i+1)
    dr = sqrt(dx*dx + dy*dy + dz*dz)
    call potptr(dr,v,fij)

    dfx = -fij * dx/dr
    dfy = -fij * dy/dr
    dfz = -fij * dz/dr
    dvdr(1,i) = dvdr(1,i) - dfx
    dvdr(2,i) = dvdr(2,i) - dfy
    dvdr(3,i) = dvdr(3,i) - dfz
    dvdr(1,i+1) = dvdr(1,i+1) + dfx
    dvdr(2,i+1) = dvdr(2,i+1) + dfy
    dvdr(3,i+1) = dvdr(3,i+1) + dfz
    vir(1,1) = vir(1,1) - dx * dfx
    vir(2,2) = vir(2,2) - dy * dfy
    vir(3,3) = vir(3,3) - dz * dfz
    vir(1,2) = vir(1,2) - dx * dfy
    vir(1,3) = vir(1,3) - dx * dfz
    vir(2,1) = vir(2,1) - dy * dfx
    vir(2,3) = vir(2,3) - dy * dfz
    vir(3,1) = vir(3,1) - dz * dfx
    vir(3,2) = vir(3,2) - dz * dfy
  enddo 

end subroutine interact_diatomic

subroutine Morse_diatomic(dr,v,dv)
  IMPLICIT NONE
  REAL(dp), INTENT(in):: dr
  REAL(dp), INTENT(out) :: v,dv
  REAL(dp) :: expr,f1,f2,expr2,expr3

  ! expr=exp(-sig*(dr-ro))
  ! v=Vo*expr*(expr-2._dp)
  ! dv=-2._dp*Vo*sig*expr*(expr-1._dp)
  
  expr=sig*(dr-ro)
  expr2=expr*expr
  expr3=expr2*expr
  f1 = 7._dp / 12._dp

  v=Vo*(expr2 - expr3 + f1*expr2*expr2)
  dv=Vo*sig*(2._dp*expr - 3._dp*expr2 + 4._dp*f1*expr3)

end subroutine Morse_diatomic

subroutine harm_diatomic(dr,v,dv)
  IMPLICIT NONE
  REAL(dp), INTENT(in):: dr
  REAL(dp), INTENT(out) :: v,dv
  REAL(dp) :: expr

  expr=dr-ro
  v=ko*expr*expr
  dv=2._dp*ko*expr

end subroutine harm_diatomic

end module diatomic
