module potential
  use kinds
  implicit none

  PRIVATE
  PUBLIC :: Pot, dPot, compute_hessian &
						,get_pot_info &
						,simulation_box

  abstract interface
		function pot_type(X) result(U)
		!! abstract interface for potential function
			import :: dp
			IMPLICIT NONE
			REAL(dp),INTENT(in)  :: X(:,:)
				!! positions of the atoms
			REAL(dp) :: U
		end function pot_type
	end interface
	
	abstract interface
		function dpot_type(X) result(dU)
		!! abstract interface for potential derivative function
			import :: dp
			IMPLICIT NONE
			REAL(dp),INTENT(in)  :: X(:,:)
				!! positions of the atoms
			REAL(dp)	:: dU(size(X,1),size(X,2))
		end function dpot_type
	end interface

	abstract interface
			function hessian_type(X) result(H)
		!! abstract interface for potential hessian
			import :: dp
			implicit none
			real(dp),intent(in)  :: X(:,:)		
				!! positions of the atoms
			real(dp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))
		end function hessian_type
	end interface

	abstract interface
		subroutine get_pot_info_type(X,Pot,Forces,hessian,vir)
		!! abstract interface for all-in-one potential info (e.g. for ab initio potentials)
			import :: dp			
			IMPLICIT NONE
			real(dp), intent(in)  :: X(:,:)
			real(dp), intent(out) :: Forces(size(X,1),size(X,2))
			real(dp), intent(out) :: Pot
			real(dp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
			real(dp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))
		end subroutine get_pot_info_type
	end interface

	TYPE SIMULATION_BOX_TYPE
		INTEGER :: ndim
		REAL(dp) :: a,b,c
		REAL(dp) :: box_lengths(3)
		REAL(dp), ALLOCATABLE :: hcell(:,:) , hcell_inv(:,:)
	CONTAINS
		PROCEDURE :: apply_pbc => simulation_box_apply_pbc
	END TYPE SIMULATION_BOX_TYPE

	TYPE(SIMULATION_BOX_TYPE), SAVE :: simulation_box

	PROCEDURE(pot_type), POINTER, SAVE :: Pot => pot_null
		!! pointer to the potential function
	PROCEDURE(dpot_type), POINTER, SAVE :: dPot => dpot_null
		!! pointer to the potential derivative function
	PROCEDURE(hessian_type), POINTER, SAVE :: compute_hessian => hessian_null
		!! pointer to the potential derivative function
	PROCEDURE(get_pot_info_type), POINTER, SAVE :: get_pot_info => get_pot_info_null
		!! pointer to the all-in-one potential function

CONTAINS

  function pot_null(X) result(U)
  !! null default potential
    IMPLICIT NONE
    REAL(dp),INTENT(in)  :: X(:,:)
      !! positions of the atoms
    REAL(dp) :: U

    U=0._dp
  end function pot_null

  function dpot_null(X) result(dU)
		!! null default dPot
    IMPLICIT NONE
    REAL(dp),INTENT(in)  :: X(:,:)
      !! positions of the atoms
    REAL(dp)	:: dU(size(X,1),size(X,2))

    dU(:,:) = 0._dp
  end function dpot_null

  function hessian_null(X) result(H)
  !! null default hessian
    implicit none
    real(dp),intent(in)  :: X(:,:)		
      !! positions of the atoms
    real(dp) :: H(size(X,1)*size(X,2),size(X,1)*size(X,2))

    H(:,:) = 0._dp
  end function hessian_null

  subroutine get_pot_info_null(X,Pot,Forces,hessian,vir)
  !! default null all-in-one potential info
    IMPLICIT NONE
    real(dp), intent(in)  :: X(:,:)
    real(dp), intent(out) :: Forces(size(X,1),size(X,2))
    real(dp), intent(out) :: Pot
    real(dp), intent(out), OPTIONAL :: vir(size(X,2),size(X,2))
    real(dp), intent(out), OPTIONAL :: hessian(size(X,1)*size(X,2),size(X,1)*size(X,2))

    Forces(:,:) = 0._dp
    Pot = 0._dp
    if(present(vir)) vir(:,:) = 0._dp
    if(present(hessian)) hessian(:,:) = 0._dp 
  end subroutine get_pot_info_null

	subroutine simulation_box_apply_pbc(self,X)
		IMPLICIT NONE
		CLASS(SIMULATION_BOX_TYPE) :: self
		REAL(dp), INTENT(inout) :: X(:,:)
		REAL(dp), ALLOCATABLE, SAVE :: q(:,:)
		INTEGER :: i
		
		IF(.NOT. ALLOCATED(q)) &
			ALLOCATE(q(size(X,2),size(X,1)))

		q=matmul(self%hcell_inv,transpose(X))
		q=q-int(q)
		X=transpose(matmul(self%hcell,q))

	end subroutine simulation_box_apply_pbc


end module potential